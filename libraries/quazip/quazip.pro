TEMPLATE = lib
CONFIG += qt warn_on

QT -= gui

# build destination config
DESTDIR = ../../build/lib
BUILD_DIR = ../../build/lib/quazip

# app configuration
DEFINES += QUAZIP_BUILD
CONFIG(staticlib): DEFINES += QUAZIP_STATIC

# sources, headers, dependencies
include(quazip.pri)

win32 {
    headers.path = $$PREFIX/include/quazip
    headers.files = $$HEADERS
    target.path = $$PREFIX/lib
    INSTALLS += headers target
    # workaround for qdatetime.h macro bug
    DEFINES += NOMINMAX
}

# common settings
#include(../../common.pri)
