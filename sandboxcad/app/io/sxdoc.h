#ifndef BIBADOCUMENT_H
#define BIBADOCUMENT_H

#include <QObject>
#include <QMetaProperty>
#include <QMap>
#include <QString>
#include <QBuffer>
#include <QByteArray>

#include "quazip.h"
#include "quazipfile.h"

class SXDoc
{
public:
    enum Mode
    {
        Create,
        Append,
        Open
    };

    SXDoc(QString file);
    ~SXDoc();

    bool open(Mode mode) const;
    void close();

    void attach(QByteArray file_content, QString name);
    void attach(QObject *object, QString name);

    QMap<QString, QMap<QString, QString> > settings() const;
    QMap<QString, QByteArray> files() const;

private:
    QuaZip *zip;
    QBuffer sb;
};

#endif // BIBADOCUMENT_H
