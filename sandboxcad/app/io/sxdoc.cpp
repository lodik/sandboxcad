#include "sxdoc.h"

SXDoc::SXDoc(QString file)
{
    zip = new QuaZip(file);
    zip->setFileNameCodec("IBM 866");
}

SXDoc::~SXDoc()
{
    if(zip->isOpen()) zip->close();

    delete zip;
}

bool SXDoc::open(SXDoc::Mode mode) const
{
    switch(mode)
    {
    case Create:
        zip->open(QuaZip::mdCreate);
        break;

    case Append:
        zip->open(QuaZip::mdAdd);
        break;

    case Open:
        zip->open(QuaZip::mdUnzip);
        break;

    default: break;
    }

    return zip->isOpen();
}

void SXDoc::close()
{
    if(zip->getMode() == QuaZip::mdAdd)
    {
        QuaZipFile qzf(zip);

        qzf.open(QIODevice::WriteOnly, QuaZipNewInfo("settings.ini"));
        sb.open(QIODevice::ReadOnly);

        qzf.write(sb.readAll());

        sb.close();
        qzf.close();
    }

    zip->close();
}

void SXDoc::attach(QByteArray file_content, QString name)
{
    QuaZipFile qzf(zip);

    qzf.open(QIODevice::WriteOnly, QuaZipNewInfo(name.section("/", -1, -1)));
    qzf.write(file_content);
    qzf.close();
}

void SXDoc::attach(QObject *object, QString name)
{
    sb.open(QIODevice::Append);

    for(int i = 0; i < object->metaObject()->propertyCount(); i++)
    {
        sb.write((QString("%1.%2:%3\n").arg(name)
                                       .arg(object->metaObject()->property(i).name())
                                       .arg(object->property(object->metaObject()->property(i).name()).toString())).toUtf8());
    }

    sb.close();
}

QMap<QString, QMap<QString, QString>> SXDoc::settings() const
{
    QStringList raw_settings;
    QMap<QString, QMap<QString, QString>> settings;

    zip->setCurrentFile("settings.ini");

    QuaZipFile qzf(zip);

    qzf.open(QIODevice::ReadOnly);
    raw_settings = QString(qzf.readAll()).split('\n');
    raw_settings.removeAll("");
    qzf.close();

    zip->setCurrentFile("");

    for(QString line : raw_settings)
    {
        QString object = line.split('.')[0];
        QString property = line.split('.')[1].split(':')[0];
        QString value = line.split('.')[1].split(':')[1];

        settings[object][property] = value;
    }

    return settings;
}

QMap<QString, QByteArray> SXDoc::files() const
{
    QMap<QString, QByteArray> fs;

    QuaZipFile qzf(zip);

    for(bool more = zip->goToFirstFile(); more; more = zip->goToNextFile())
    {
        if(zip->getCurrentFileName() != "settings.ini")
        {
            qzf.open(QIODevice::ReadOnly);
            fs[zip->getCurrentFileName()] = qzf.readAll();
            qzf.close();
        }
    }

    return fs;
}
