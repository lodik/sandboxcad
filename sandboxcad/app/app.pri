HEADERS += \
        # mainwindow
        $$PWD/ui/mainwindow.h \
        $$PWD/ui/dialogs/mapping.h \
        # widgets
        $$PWD/ui/widgets/view.h \
        $$PWD/ui/widgets/scene.h \
        $$PWD/ui/widgets/scene_modules/abstractscenemodule.h \
        $$PWD/ui/widgets/scene_modules/shaded.h \
        $$PWD/ui/widgets/scene_modules/wireframe.h \
        $$PWD/ui/widgets/dropdownmenu.h \
        # primitive dialogs
        $$PWD/ui/dialogs/cubedialog.h \
        $$PWD/ui/dialogs/spheredialog.h \
        $$PWD/ui/dialogs/conedialog.h \
        $$PWD/ui/dialogs/cylinderdialog.h \
        # boolean ops dialogs
        # rendering
        $$PWD/render/mesh.h \
        $$PWD/render/meshfunctor.h \
        $$PWD/render/wireframefunctor.h \
        # file io
        $$PWD/io/sxdoc.h \
    $$PWD/ui/widgets/scene_modules/csys.h \
    $$PWD/ui/dialogs/booleandialog.h \
    $$PWD/ui/settings.h \
    $$PWD/ui/widgets/scene_modules/highlight.h

SOURCES += \
        # mainwindow
        $$PWD/ui/mainwindow.cpp \
        # widgets
        $$PWD/ui/widgets/view.cpp \
        $$PWD/ui/widgets/scene.cpp \
        $$PWD/ui/widgets/scene_modules/abstractscenemodule.cpp \
        $$PWD/ui/widgets/scene_modules/shaded.cpp \
        $$PWD/ui/widgets/scene_modules/wireframe.cpp \
        $$PWD/ui/widgets/dropdownmenu.cpp \
        # primitive dialogs
        $$PWD/ui/dialogs/cubedialog.cpp \
        $$PWD/ui/dialogs/spheredialog.cpp \
        $$PWD/ui/dialogs/conedialog.cpp \
        $$PWD/ui/dialogs/cylinderdialog.cpp \
        # boolean ops dialogs
        # rendering
        $$PWD/render/mesh.cpp \
        $$PWD/render/meshfunctor.cpp \
        $$PWD/render/wireframefunctor.cpp \
        # file io
        $$PWD/io/sxdoc.cpp \
    $$PWD/ui/widgets/scene_modules/csys.cpp \
    $$PWD/ui/dialogs/booleandialog.cpp \
    $$PWD/ui/settings.cpp \
    $$PWD/ui/widgets/scene_modules/highlight.cpp

FORMS += \
        # mainwindow
        $$PWD/ui/forms/mainwindow.ui \
        # primitive bodies dialogs
        $$PWD/ui/forms/cubedialog.ui \
        $$PWD/ui/forms/spheredialog.ui \
        $$PWD/ui/forms/conedialog.ui \
        $$PWD/ui/forms/cylinderdialog.ui \
        # boolean ops dialogs
    $$PWD/ui/forms/booleandialog.ui \
    $$PWD/ui/forms/settingsdialog.ui
