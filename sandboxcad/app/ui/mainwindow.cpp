#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "parasolid/demo.h"
#include "parasolid/entitymanager.h"
#include "parasolid/operations/blockoperations.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    view(new View(this->windowHandle())),
    styles(new StylesDropDownMenu(this))//,
    //settings(new Settings(this))
{
    ui->setupUi(this);

    ui->horizontalLayout->addWidget(QWidget::createWindowContainer(view, this));
    ui->toolbar->addWidget(styles);

    statusBar()->showMessage("");

    QObject::connect(&pk::EntityManager::Instance().getBuildTree(), &pk::BuildTree::operationAdded, [this]() {
        ui->tree->clear();
        ui->tree->setColumnCount(1);
        auto historyItems = pk::EntityManager::Instance().getBuildTree().getOperations();
        QList<QTreeWidgetItem *> items;
        for (pk::Operation op : historyItems) {
            items.append(new QTreeWidgetItem(ui->tree, QStringList(QString("item: %1").arg(op.operationName()))));
        }
        ui->tree->insertTopLevelItems(0, items);
    });

    partmgr.setCurrentPartition(partmgr.createPartition("__default"));
    pk::Operation op("New document", { });
    pk::EntityManager::Instance().getBuildTree().addOperation(op);

    initCallbacks();

    map_primitive(cube, Cube, BlockOperations::createSolidBlock(dialogs.cube->dimensions().x(), dialogs.cube->dimensions().y(), dialogs.cube->dimensions().z(), &ax, &body));
    map_primitive(sphere, Sphere, BlockOperations::createSolidSphere(dialogs.sphere->radius(), &ax, &body));
    map_primitive(cone, Cone, BlockOperations::createSolidCone(dialogs.cone->lradius(), dialogs.cone->height(), dialogs.cone->angle(), &ax, &body));
    map_primitive(cylinder, Cylinder, BlockOperations::createSolidCylinder(dialogs.cylinder->radius(), dialogs.cylinder->height(), &ax, &body));

    map_boolean(unite, "Объединение");
    map_boolean(subtract, "Вычитание");
    map_boolean(intersect, "Пересечение");

    QObject::connect(ui->cb_filter, static_cast<void (QComboBox::*)(int)>(&QComboBox::activated), this, [=](int index) {
        ui->list_elements->clear();

        switch(index) {
        case 0: // тела
            for(auto body : pk::EntityManager::Instance().getBodies()) {
                auto e = new QListWidgetItem(QString("Тело %1").arg(QString::number(body)), ui->list_elements);
                e->setData(Qt::UserRole, QVariant::fromValue(body));
            }
            break;
        case 1: // ребра
            for(auto edge : pk::EntityManager::Instance().getEdges()) {
                auto e = new QListWidgetItem(QString("Ребро %1").arg(QString::number(edge)), ui->list_elements);
                e->setData(Qt::UserRole, QVariant::fromValue(edge));
            }
            break;
        case 2: // грани
            // И тут тоже как-нибудь так. Или по-другому.
            break;
        }

        ui->label_elements->setText(QString::number(ui->list_elements->count()));
    });

    QObject::connect(ui->list_elements, &QListWidget::itemSelectionChanged, view->scene()->module("highlight"), [=](){
        static_cast<HighlightModule *>(view->scene()->module("highlight"))->reset();
    });

    QObject::connect(ui->list_elements, &QListWidget::itemClicked, view->scene()->module("highlight"), [=](QListWidgetItem *item){
        PK_CLASS_t eclass;
        PK_ENTITY_t element = item->data(Qt::UserRole).value<PK_ENTITY_t>();
        PK_ENTITY_ask_class(element, &eclass);
        qDebug() << eclass;
        switch(eclass)
        {
        case PK_CLASS_body:
        case PK_CLASS_edge:
        case PK_CLASS_face:
            static_cast<HighlightModule *>(view->scene()->module("highlight"))->highlight(element);
            view->update();
            break;
        default:
            break;
        }
    });

    if(!appSettings.enable_highlight) {
        ui->list_elements->disconnect(view->scene()->module("highlight"));
        view->scene()->disableModule("highlight");
    }

    QObject::connect(&Settings::instance(), &Settings::accepted, view, &View::update);
}

MainWindow::~MainWindow()
{
    LOG(DEBUG) << "bye";

    delete ui;
}

void MainWindow::keyPressEvent(QKeyEvent *e)
{
    switch(e->key())
    {
    case Qt::Key_F5:
        view->update();
        break;
    case Qt::Key_T:
        break;
    default:
        break;
    }

    QMainWindow::keyPressEvent(e);
}

void MainWindow::initCallbacks()
{
    // menu callbacks

    QObject::connect(ui->action_new, &QAction::triggered, this, [=]() {
        QString path = QFileDialog::getSaveFileName(this, "Новый", qApp->applicationDirPath(), "SandboxCAD documents (*.sx)");

        if(path.isEmpty()) return;

        QString name = path.split(QDir::separator()).last();

        pk::Partition partition = partmgr.createPartition(name);
        partmgr.setCurrentPartition(partition);

        for(auto action : ui->menu_scenes->actions())
            action->setChecked(false);

        ui->menu_scenes->addAction(name);
        ui->menu_scenes->actions().last()->setCheckable(true);
        ui->menu_scenes->actions().last()->setChecked(true);

        static_cast<HighlightModule *>(view->scene()->module("highlight"))->reset();
        emit ui->cb_filter->activated(ui->cb_filter->currentIndex());
        view->update();

        QObject::connect(ui->menu_scenes->actions().last(), &QAction::triggered, this, [=](){
            for(auto action : ui->menu_scenes->actions())
                action->setChecked(false);

            static_cast<QAction *>(sender())->setChecked(true);

            partmgr.setCurrentPartition(partition);

            static_cast<HighlightModule *>(view->scene()->module("highlight"))->reset();
            emit ui->cb_filter->activated(ui->cb_filter->currentIndex());
            view->update();
        });
    });

    QObject::connect(ui->action_open, &QAction::triggered, this, [=]() {
        QString filename = QFileDialog::getOpenFileName(this, "Открыть", qApp->applicationDirPath(), "SandboxCAD documents (*.sx)");
    });

    //QObject::connect(ui->action_save, &QAction::triggered, this, [=]() {});

    QObject::connect(ui->action_saveas, &QAction::triggered, this, [=]() {
        QString filename = QFileDialog::getSaveFileName(this, "Сохранить как", qApp->applicationDirPath(), "SandboxCAD documents (*.sx)");
        if(!filename.isEmpty())
            PSaver::save(filename);
    });

    QObject::connect(ui->action_fullscreen, &QAction::triggered, this, [=](bool checked) {
        if(checked)
            showFullScreen();
        else showNormal();
    });

    QObject::connect(ui->action_appsettings, &QAction::triggered, this, [=](){
         Settings::instance().exec();
    });

    QObject::connect(ui->action_about, &QAction::triggered, this, [=]() {
       QMessageBox::about(this, "О программе", "Нужно написать побольше всего, чтобы окошко большое было");
    });

    QObject::connect(ui->action_aboutqt, &QAction::triggered, [this]() {
        QMessageBox::aboutQt(this, "О Qt");
    });

    // toolbar/styles

    QObject::connect(styles, &StylesDropDownMenu::shaded, this, [=]() {
        view->scene()->disableModule("wireframe");
        view->scene()->enableModule("shaded");

        view->update();
    });

    QObject::connect(styles, &StylesDropDownMenu::wireframe, this, [=]() {
        view->scene()->disableModule("shaded");
        view->scene()->enableModule("wireframe");

        view->update();
    });

    QObject::connect(styles, &StylesDropDownMenu::all, this, [=]() {
        view->scene()->enableModule("shaded");
        view->scene()->enableModule("wireframe");

        view->update();
    });

    // toolbar/demo

    QObject::connect(ui->actionDemo, &QAction::triggered, [=]() {
        Demo::demo2();
        view->update();
    });

    QObject::connect(ui->actionDrawing, &QAction::toggled, this, [=](bool checked) {
        view->setMode(View::DRAWING);
    });

    QObject::connect(ui->actionPicking, &QAction::toggled, this, [=](bool checked) {
        view->setMode(View::PICKING);
    });
}
