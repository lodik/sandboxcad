#ifndef DROPDOWNMENU_H
#define DROPDOWNMENU_H

#include <QToolButton>
#include <QMenu>
#include <QWidgetAction>
#include <QVBoxLayout>
#include <QList>
#include <QString>

class StylesDropDownMenu : public QToolButton
{
    Q_OBJECT

public:
    explicit StylesDropDownMenu(QWidget *parent = 0);
    ~StylesDropDownMenu();

signals:
    void shaded();
    void wireframe();
    void all();

private:
    QWidget *m_inner_form;
    QMap<QString, QToolButton *> m_buttons;
};

#endif // DROPDOWNMENU_H
