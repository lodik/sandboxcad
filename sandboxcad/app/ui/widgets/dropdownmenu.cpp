#include "dropdownmenu.h"

#define add_style_button(style, tooltip)\
    m_buttons[#style] = new QToolButton(m_inner_form);\
    m_buttons[#style]->setIcon(QIcon(":/icons/flat/style_"#style));\
    m_buttons[#style]->setToolTip(tooltip);\
    m_buttons[#style]->setStatusTip(tooltip);\
    \
    QObject::connect(m_buttons[#style], &QToolButton::clicked, this, [=](){\
        setIcon(m_buttons[#style]->icon());\
        emit this->style();\
    });\
    \
    m_inner_form->layout()->addWidget(m_buttons[#style]);\

StylesDropDownMenu::StylesDropDownMenu(QWidget *parent) :
    QToolButton(parent),
    m_inner_form(new QWidget(this))
{
    setPopupMode(QToolButton::InstantPopup);
    setMenu(new QMenu(this));
    setCheckable(true);
    setStatusTip("Изменение стиля отображения");

    m_inner_form->setLayout(new QVBoxLayout(m_inner_form));

    add_style_button(shaded, "Затененное");
    add_style_button(wireframe, "Ребра");
    add_style_button(all, "Затененное с ребрами");

    auto action = new QWidgetAction(this);
    action->setDefaultWidget(m_inner_form);

    menu()->addAction(action);

    m_buttons["all"]->click();
}

StylesDropDownMenu::~StylesDropDownMenu()
{

}

