#ifndef VIEW_H
#define VIEW_H

#include <QWindow>
#include <QOpenGLContext>

#include <QResizeEvent>
#include <QWheelEvent>
#include <QMouseEvent>
#include <QKeyEvent>

#include <Qt3DCore/QAspectEngine>
#include <Qt3DCore/QCamera>

#include <Qt3DInput/QInputAspect>

#include <Qt3DRenderer/QRenderAspect>
#include <Qt3DRenderer/QForwardRenderer>
#include <Qt3DRenderer/QFrameGraph>
#include <Qt3DRenderer/QClearBuffer>

#include "easylogging++.h"

#include "scene.h"

class View : public QWindow
{
    Q_OBJECT

public:
    enum MODE {
        NONE = 1,
        PICKING = 2,
        DRAWING = 3
    };

    explicit View(QWindow* parent = 0);
    ~View();

    Scene *scene() const;

    MODE mode() const;
    void setMode(const MODE &mode);

public slots:
    void update();

protected:
    void resizeEvent(QResizeEvent *e);
    void wheelEvent(QWheelEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void keyPressEvent(QKeyEvent *e);

private:
    Qt3D::QInputAspect *input() const;

private:
    // он будет течь, но если попытаться его удалить, получится бесконечный цикл
    Qt3D::QAspectEngine *m_engine;

    Scene *m_scene;

    Qt3D::QForwardRenderer *m_renderer;
    Qt3D::QFrameGraph *m_framegraph;

    Qt3D::QCamera *m_default_camera;
    MODE m_mode;
};

#endif // VIEW_H
