#ifndef CSYSMODULE_H
#define CSYSMODULE_H

#include <Qt3DCore/QEntity>
#include <Qt3DRenderer/QPerVertexColorMaterial>

#include <QVector3D>
#include <QList>
#include <QPair>

#include "abstractscenemodule.h"

#include "app/render/mesh.h"

class CSysModule : public AbstractSceneModule
{
    Q_OBJECT

public:
    explicit CSysModule(Qt3D::QNode *parent = 0);
    ~CSysModule();

    void update();

protected:
    void construct();
    void clear();

private:
    QList<Qt3D::QEntity *>m_csys_entities;
};

#endif // CSYSMODULE_H
