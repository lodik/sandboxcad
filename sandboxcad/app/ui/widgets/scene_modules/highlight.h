#ifndef HIGHLIGHTMODULE_H
#define HIGHLIGHTMODULE_H

#include "abstractscenemodule.h"
#include "app/render/mesh.h"

#include "parasolid/frustrum/frustrumgo.h"

#include <QColor>
#include <QVector>
#include <Qt3DRenderer/QPerVertexColorMaterial>

class HighlightModule : public AbstractSceneModule
{
public:
    explicit HighlightModule(Qt3D::QNode *parent = 0);
    ~HighlightModule();

    void update();

public slots:
    void highlight(PK_BODY_t body);
    void reset();

protected:
    void construct();
    void clear();

private:
    QColor m_color;
    QList<Qt3D::QEntity *> m_entities;
    QVector<PK_BODY_t> m_bodies;
};

#endif // HIGHLIGHTMODULE_H
