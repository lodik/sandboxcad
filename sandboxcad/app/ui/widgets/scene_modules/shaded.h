#ifndef SHADED_H
#define SHADED_H

#include <Qt3DCore/QEntity>
#include <Qt3DCore/QCamera>

#include <Qt3DRenderer/QFrameGraph>
#include <Qt3DRenderer/QForwardRenderer>
#include <Qt3DRenderer/QMesh>
#include <Qt3DRenderer/QMeshData>
#include <Qt3DRenderer/QPerVertexColorMaterial>
#include <Qt3DRenderer/QPhongMaterial>
#include <Qt3DRenderer/QMaterial>

#include <QTransform>

#include <QChildEvent>
#include <QColor>
#include <QVector>

#include "parasolid/entitymanager.h"
#include "parasolid/frustrum/frustrumgo.h"
#include "app/render/mesh.h"

#include "easylogging++.h"
#include "abstractscenemodule.h"

#include "app/ui/settings.h"

#include <QDebug>

class ShadedModule : public AbstractSceneModule
{
    Q_OBJECT

public:
    explicit ShadedModule(Qt3D::QNode *parent = 0);
    ~ShadedModule();

    void update();

protected:
    void construct();
    void clear();

private:
    QList<Qt3D::QEntity *> m_entities;
};

#endif // SHADED_H
