#ifndef SCENEMODULE_H
#define SCENEMODULE_H

#include <Qt3DCore/QEntity>
#include <Qt3DRenderer/QMaterial>
#include <QVector>
#include <QPair>

#include "easylogging++.h"

#include "app/render/mesh.h"

class AbstractSceneModule : public Qt3D::QEntity
{
    Q_OBJECT

public:
    explicit AbstractSceneModule(Qt3D::QNode *parent = 0);
    ~AbstractSceneModule();

    virtual void update() = 0;
};

#endif // SCENEMODULE_H
