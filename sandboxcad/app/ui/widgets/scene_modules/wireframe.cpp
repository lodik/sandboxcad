#include "wireframe.h"

WireframeModule::WireframeModule(Qt3D::QNode *parent) :
    AbstractSceneModule(parent)
{

}

WireframeModule::~WireframeModule()
{
    LOG(DEBUG) << "bye";

    clear();
}

void WireframeModule::update()
{
    LOG(DEBUG) << "started";

    clear();
    construct();

    for(auto entity : m_entities)
        entity->setParent(this);

    LOG(DEBUG) << "finished";
}

void WireframeModule::construct()
{
    LOG(DEBUG) << "started";

    auto bodies = FrustrumGO::getBodiesInfo(pk::EntityManager::Instance().getBodies());

    auto *material = new Qt3D::QPerVertexColorMaterial();

    for(auto body : bodies) {
        for(QVector<double> edge : body.wire()) {
            QVector<double> colors;
            for(int i = 0; i < edge.size() / 3; i++) {
                colors.push_back(0);
                colors.push_back(0);
                colors.push_back(0);
            }

            auto mesh = new Mesh(Mesh::Type::WIREFRAME);
            mesh->setVertices(edge);
            mesh->setColors(colors);

            auto entity = new Qt3D::QEntity();
            entity->addComponent(mesh);
            entity->addComponent(static_cast<Qt3D::QMaterial *>(material));

            m_entities.push_back(entity);
        }
    }

    delete material;

    LOG(DEBUG) << "finished";
}

void WireframeModule::clear()
{
    LOG(DEBUG) << "remove " << m_entities.count() << " entities";

    qDeleteAll(m_entities);

    m_entities.clear();
}
