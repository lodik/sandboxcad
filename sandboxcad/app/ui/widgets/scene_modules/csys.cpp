#include "csys.h"

CSysModule::CSysModule(Qt3D::QNode *parent) :
    AbstractSceneModule(parent)
{

}

CSysModule::~CSysModule()
{
    LOG(DEBUG) << "bye";

    clear();
}

void CSysModule::update()
{
    LOG(DEBUG) << "started";

    clear();
    construct();

    for(auto entity : m_csys_entities)
        entity->setParent(this);

    LOG(DEBUG) << "finished";
}

void CSysModule::construct()
{
    LOG(DEBUG) << "started";

    QVector3D origin(0, 0, 0);
    double pts[9] = { 10, 0, 0, 0, 10, 0, 0, 0, 10 };
    double clrs[9] = { 128, 0, 0, 0, 128, 0, 0, 0, 128 };


    for(int i = 0; i < 3; i++) {
        QVector<double> points;
        QVector<double> colors;
        Qt3D::QMaterial *material = new Qt3D::QPerVertexColorMaterial();

        points.push_back(origin.x());
        points.push_back(origin.y());
        points.push_back(origin.z());

        points.push_back(origin.x() + pts[i * 3 + 0]);
        points.push_back(origin.y() + pts[i * 3 + 1]);
        points.push_back(origin.z() + pts[i * 3 + 2]);

        colors.push_back(clrs[i * 3 + 0]);
        colors.push_back(clrs[i * 3 + 1]);
        colors.push_back(clrs[i * 3 + 2]);

        colors.push_back(clrs[i * 3 + 0]);
        colors.push_back(clrs[i * 3 + 1]);
        colors.push_back(clrs[i * 3 + 2]);

        auto mesh = new Mesh(Mesh::Type::WIREFRAME);
        mesh->setVertices(points);
        mesh->setColors(colors);

        auto entity = new Qt3D::QEntity();
        entity->addComponent(mesh);
        entity->addComponent(material);

        m_csys_entities.append(entity);
    }


    LOG(DEBUG) << "finished";
}

void CSysModule::clear()
{
    LOG(DEBUG) << "remove " << m_csys_entities.count() << " entities";

    qDeleteAll(m_csys_entities);

    m_csys_entities.clear();
}
