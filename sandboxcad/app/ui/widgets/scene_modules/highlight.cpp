#include "highlight.h"

HighlightModule::HighlightModule(Qt3D::QNode *parent) :
    AbstractSceneModule(parent),
    m_color(128, 0, 0)
{

}

HighlightModule::~HighlightModule()
{
    LOG(DEBUG) << "bye";

    clear();
}

void HighlightModule::update()
{
    LOG(DEBUG) << "started";

    clear();
    construct();

    for(auto entity : m_entities)
        entity->setParent(this);

    LOG(DEBUG) << "finished";
}

void HighlightModule::highlight(PK_BODY_t body)
{
    m_bodies.push_back(body);
}

void HighlightModule::reset()
{
    m_bodies.clear();
}

void HighlightModule::construct()
{
    LOG(DEBUG) << "started";

    auto bodies = FrustrumGO::getBodiesInfo(m_bodies);

    auto *material = new Qt3D::QPerVertexColorMaterial();

    for(auto body : bodies) {
        QVector<double> vertices = body.vertexes();
        QVector<double> normals = body.normals();
        QVector<double> colors;

        for(int i = 0; i < vertices.size() / 3; i++) {
            colors.push_back(appSettings.highlight_color.redF());
            colors.push_back(appSettings.highlight_color.greenF());
            colors.push_back(appSettings.highlight_color.blueF());
        }

        auto mesh = new Mesh();
        mesh->setVertices(vertices);
        mesh->setNormals(normals);
        mesh->setColors(colors);

        auto entity = new Qt3D::QEntity();
        entity->addComponent(mesh);
        entity->addComponent(static_cast<Qt3D::QMaterial *>(material));

        m_entities.push_back(entity);
    }

    LOG(DEBUG) << "finished";
}

void HighlightModule::clear()
{
    LOG(DEBUG) << "remove " << m_entities.count() << " entities";

    qDeleteAll(m_entities);

    m_entities.clear();
}

