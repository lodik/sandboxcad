#ifndef WIREFRAME_H
#define WIREFRAME_H

#include <Qt3DCore/QEntity>
#include <Qt3DCore/QCamera>

#include <Qt3DRenderer/QFrameGraph>
#include <Qt3DRenderer/QForwardRenderer>
#include <Qt3DRenderer/QMesh>
#include <Qt3DRenderer/QMeshData>
#include <Qt3DRenderer/QPerVertexColorMaterial>
#include <Qt3DRenderer/QPhongMaterial>

#include <QTransform>

#include <QChildEvent>

#include <QVector>

#include "parasolid/entitymanager.h"
#include "parasolid/frustrum/frustrumgo.h"
#include "app/render/mesh.h"
#include "abstractscenemodule.h"
#include "easylogging++.h"

#include <QDebug>

class WireframeModule : public AbstractSceneModule
{
    Q_OBJECT

public:
    explicit WireframeModule(Qt3D::QNode *parent = 0);
    ~WireframeModule();

    void update();

protected:
    void construct();
    void clear();

private:
    QList<Qt3D::QEntity *> m_entities;
};

#endif // WIREFRAME_H
