#include "shaded.h"

ShadedModule::ShadedModule(Qt3D::QNode *parent) :
    AbstractSceneModule(parent)
{

}

ShadedModule::~ShadedModule()
{
    LOG(DEBUG) << "bye";

    clear();
}

void ShadedModule::update()
{
    LOG(DEBUG) << "started";

    clear();
    construct();

    for(auto entity : m_entities)
        entity->setParent(this);

    LOG(DEBUG) << "finished";
}

void ShadedModule::construct()
{
    LOG(DEBUG) << "started";

    auto bodies = FrustrumGO::getBodiesInfo(pk::EntityManager::Instance().getBodies());

    auto *material = new Qt3D::QPerVertexColorMaterial();

    for(auto body : bodies) {
        QVector<double> vertices = body.vertexes();
        QVector<double> normals = body.normals();
        QVector<double> colors;

        for(int i = 0; i < vertices.size() / 3; i++) {
            colors.push_back(appSettings.body_color.redF());
            colors.push_back(appSettings.body_color.greenF());
            colors.push_back(appSettings.body_color.blueF());
        }

        auto mesh = new Mesh();
        mesh->setVertices(vertices);
        mesh->setNormals(normals);
        mesh->setColors(colors);

        auto entity = new Qt3D::QEntity();
        entity->addComponent(mesh);
        entity->addComponent(static_cast<Qt3D::QMaterial *>(material));

        m_entities.push_back(entity);
    }

    LOG(DEBUG) << "finished";
}

void ShadedModule::clear()
{
    LOG(DEBUG) << "remove " << m_entities.count() << " entities";

    qDeleteAll(m_entities);

    m_entities.clear();
}
