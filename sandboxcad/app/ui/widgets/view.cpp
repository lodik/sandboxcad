#include "view.h"
#include <Qt3DCore/QRay3D>
#include <QPainter>
#include "parasolid/ppicker.h"

View::View(QWindow *parent) :
    QWindow(parent),
    m_engine(new Qt3D::QAspectEngine),
    m_scene(new Scene),
    m_renderer(new Qt3D::QForwardRenderer),
    m_framegraph(new Qt3D::QFrameGraph(m_scene)),
    m_default_camera(new Qt3D::QCamera(m_scene))
{
    setSurfaceType(QSurface::OpenGLSurface);

    LOG(DEBUG) << "OpenGL detecting...";

    QSurfaceFormat format;
    if (QOpenGLContext::openGLModuleType() == QOpenGLContext::LibGL) {
        format.setVersion(4, 3);
        format.setProfile(QSurfaceFormat::CoreProfile);
    }
    format.setDepthBufferSize(24);
    format.setGreenBufferSize(8);
    format.setBlueBufferSize(8);
    format.setRedBufferSize(8);
    format.setAlphaBufferSize(0);
    format.setStencilBufferSize(8);
    format.setSamples(16);
    format.setSwapBehavior(QSurfaceFormat::DoubleBuffer);
    setFormat(format);
    create();

    LOG(DEBUG) << "Using OpenGL" << format.version() << " profile";

    LOG(DEBUG) << "AspectEngine initialization...";

    m_engine->registerAspect(new Qt3D::QRenderAspect());
    m_engine->registerAspect(new Qt3D::QInputAspect());

    QVariantMap data;
    data.insert(QStringLiteral("surface"), QVariant::fromValue(static_cast<QSurface *>(this)));
    data.insert(QStringLiteral("eventSource"), QVariant::fromValue(this));
    m_engine->setData(data);

    m_engine->initialize();

    LOG(DEBUG) << "Setting up rendering and scene components...";

    m_default_camera->setProperty("zoomFactor", QVariant::fromValue(1.0f));
    m_default_camera->lens()->setOrthographicProjection(-width()/32, width()/32, -height()/32, height()/32, -1000.0f, 1000.0f);
    m_default_camera->setPosition(QVector3D(0, 0, 5));
    m_default_camera->setUpVector(QVector3D(0, 1, 0));
    m_default_camera->setViewCenter(QVector3D(0, 0, 0));

    input()->setCamera(m_default_camera);

    m_renderer->setClearColor(QColor(200, 210, 230));
    m_renderer->setCamera(m_default_camera);

    m_framegraph->setActiveFrameGraph(m_renderer);

    m_scene->addComponent(m_framegraph);

    update();

    LOG(DEBUG) << "OK";
}

View::~View()
{
    LOG(DEBUG) << "bye";
}

Scene *View::scene() const
{
    return m_scene;
}

void View::update()
{
    LOG(DEBUG) << "started";

    m_scene->update();

    m_engine->setRootEntity(m_scene);

    LOG(DEBUG) << "finished";
}

Qt3D::QInputAspect *View::input() const
{
    return static_cast<Qt3D::QInputAspect *>(m_engine->aspects()[1]);
}

View::MODE View::mode() const
{
    return m_mode;
}

void View::setMode(const View::MODE &mode)
{
    m_mode = mode;
}

void View::resizeEvent(QResizeEvent *e)
{
    m_default_camera->lens()->setOrthographicProjection(-width()/32  * m_default_camera->property("zoomFactor").value<float>(),
                                                        width()/32   * m_default_camera->property("zoomFactor").value<float>(),
                                                        -height()/32  * m_default_camera->property("zoomFactor").value<float>(),
                                                        height()/32 * m_default_camera->property("zoomFactor").value<float>(),
                                                        -1000.0f, 1000.0f);

    QWindow::resizeEvent(e);
}

void View::wheelEvent(QWheelEvent *e)
{
    float zoomfactor = m_default_camera->property("zoomFactor").value<float>();

    if(e->delta() > 0) {
        if(zoomfactor > 0.05)
            m_default_camera->setProperty("zoomFactor", zoomfactor - 0.05);
    }
    else if(e->delta() < 0) {
        m_default_camera->setProperty("zoomFactor", zoomfactor + 0.05);
    }

    zoomfactor = m_default_camera->property("zoomFactor").value<float>();

    m_default_camera->lens()->setOrthographicProjection(-width()/32  * zoomfactor,
                                                         width()/32  * zoomfactor,
                                                        -height()/32 * zoomfactor,
                                                         height()/32 * zoomfactor,
                                                        -1000.0f, 1000.0f);

    QWindow::wheelEvent(e);
}

void View::mousePressEvent(QMouseEvent *e)
{
    LOG(DEBUG) << "User press the mouse button!";
    static QPoint firstPoint;
    static QPoint secondPoint;
    switch (m_mode) {
    case PICKING:

        break;
    case DRAWING:
        if (e->button() == Qt::LeftButton) {
            firstPoint = this->mapFromGlobal(QCursor::pos());
            LOG(TRACE) << "First click at [" << firstPoint.x() << ", " << firstPoint.y() << "]";
        } else if (e->button() == Qt::RightButton) {
            secondPoint = this->mapFromGlobal(QCursor::pos());
            LOG(TRACE) << "Second click at [" << secondPoint.x() << ", " << secondPoint.y() << "]";
        }

        if (!firstPoint.isNull() && !secondPoint.isNull()) {
            PK_LINE_sf_s lineOpt;
            QVector3D fp(firstPoint);
            QVector3D sp(secondPoint);

            QVector3D direction = sp - fp;
            QVector3D normDir = direction.normalized();
            lineOpt.basis_set.axis = PK_VECTOR1_t { normDir.x(), normDir.y(), normDir.z() };
            lineOpt.basis_set.location = PK_VECTOR_t { fp.x(), fp.y(), 0 };

            PK_LINE_t line;
            PK_LINE_create(&lineOpt, &line);

            PK_CURVE_make_wire_body_o_t wireBodyOptions;
            PK_CURVE_make_wire_body_o_m(wireBodyOptions);
            wireBodyOptions.want_edges = PK_LOGICAL_true;
            PK_BODY_t wireBody;
            int nEdges = 0;
            PK_EDGE_t *edges;
            PK_INTERVAL_t interval;
            interval.value[0] = 0.0;
            interval.value[1] = direction.length();

            PK_CURVE_make_wire_body_2(1, &line, &interval, &wireBodyOptions, &wireBody, &nEdges, &edges, nullptr);

            firstPoint = QPoint();
            secondPoint = QPoint();
        }
        break;
    default:

        break;
    }
    QWindow::mousePressEvent(e);
}

void View::keyPressEvent(QKeyEvent *e)
{
    switch(e->key())
    {
    case Qt::Key_F5:
        update();
        break;
    case Qt::Key_P: {
        QVector3D pos(mapFromGlobal(QCursor::pos()));
        QVector3D wpos(pos.x() / ((float)width()/32.f) - 16.f, pos.y() / (-(float)height()/32.f) + 16.f, 0);
        input()->camera()->translate(wpos);
        break;
    }
    default:
        break;
    }

    QWindow::keyPressEvent(e);
}
