#ifndef SCENE_H
#define SCENE_H

#include <QList>
#include <QMap>
#include <QString>

#include <Qt3DCore/QEntity>

#include "easylogging++.h"

#include "scene_modules/abstractscenemodule.h"
#include "scene_modules/shaded.h"
#include "scene_modules/wireframe.h"
#include "scene_modules/csys.h"
#include "scene_modules/highlight.h"

class Scene : public Qt3D::QEntity
{
    Q_OBJECT

public:
    explicit Scene(Qt3D::QNode *parent = 0);
    ~Scene();

    void enableModule(QString module);
    void disableModule(QString module);

    QList<QString> activeModules();

    AbstractSceneModule *module(QString name) const;

    void update();

private:
    struct moduleinfo {
        QString name;
        bool enabled;
    };

private:
    QMap<moduleinfo *,  AbstractSceneModule *> m_modules;
};

#endif // SCENE_H
