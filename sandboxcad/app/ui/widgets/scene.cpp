#include "scene.h"

Scene::Scene(Qt3D::QNode *parent) :
    Qt3D::QEntity(parent)
{
    LOG(DEBUG) << "Scene creating...";

    m_modules[new moduleinfo {"shaded", true}] = new ShadedModule(this);
    m_modules[new moduleinfo {"wireframe", true}] = new WireframeModule(this);

    m_modules[new moduleinfo {"csys", true}] = new CSysModule(this);

    m_modules[new moduleinfo {"highlight", true}] = new HighlightModule(this);

    LOG(DEBUG) << "OK";
}

Scene::~Scene()
{
    LOG(DEBUG) << "remove " << m_modules.count() << " modules";

    qDeleteAll(m_modules);
}

void Scene::enableModule(QString module)
{
    LOG(DEBUG) << "module " << module;

    for(moduleinfo *mi : m_modules.keys())
        if(mi->name == module)
            mi->enabled = true;
}

void Scene::disableModule(QString module)
{
    LOG(DEBUG) << "module " << module;

    for(moduleinfo *mi : m_modules.keys())
        if(mi->name == module) {
            mi->enabled = false;
            m_modules[mi]->setParent(0);
        }
}

QList<QString> Scene::activeModules()
{
    QList<QString> active_modules;

    for(moduleinfo *mi : m_modules.keys())
        if(mi->enabled)
            active_modules.append(mi->name);

    return active_modules;
}

AbstractSceneModule *Scene::module(QString name) const
{
    for(moduleinfo *mi : m_modules.keys())
        if(mi->name == name)
            return m_modules[mi];

    return nullptr;
}

void Scene::update()
{
    LOG(DEBUG) << "active modules " << activeModules();

    for(moduleinfo *mi : m_modules.keys())
        if(mi->enabled) {
            m_modules[mi]->update();
            m_modules[mi]->setParent(this);
        }
}
