#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QFileDialog>
#include <QComboBox>
#include <QListWidgetItem>
#include <QKeyEvent>
#include <QDir>

#include "easylogging++.h"

#include "widgets/view.h"

#include "settings.h"

#include "dialogs/cubedialog.h"
#include "dialogs/spheredialog.h"
#include "dialogs/cylinderdialog.h"
#include "dialogs/conedialog.h"

#include "dialogs/booleandialog.h"
//#include "dialogs/subtractdialog.h"
//#include "dialogs/intersectdialog.h"

#include "dialogs/mapping.h"

#include "widgets/dropdownmenu.h"

#include "parasolid/io/psaver.h"
#include "parasolid/partitionmanager.h"

Q_DECLARE_METATYPE(PK_AXIS2_sf_s)
Q_DECLARE_METATYPE(PK_BODY_t)

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void keyPressEvent(QKeyEvent *e);

private:
    void initCallbacks();

private:
    Ui::MainWindow *ui;
    View *view;
    StylesDropDownMenu *styles;

    //Settings *settings;

    struct {
        CubeDialog *cube;
        SphereDialog *sphere;
        CylinderDialog *cylinder;
        ConeDialog *cone;

        BooleanDialog *unite;
        BooleanDialog *subtract;
        BooleanDialog *intersect;
    } dialogs;
};

#endif // MAINWINDOW_H
