#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>
#include <QColor>
#include <QDoubleSpinBox>
#include <QColorDialog>

#include "easylogging++.h"

namespace Ui {
class Settings;
}

#define appSettings Settings::instance().params()

class Settings : public QDialog
{
    Q_OBJECT

private:
    struct params_t {
        double tolerance = 0.01;
        QColor body_color = QColor(100, 125, 170);
        QColor highlight_color = QColor(128, 0, 0);
        bool enable_highlight = true;
    };

public:
    static Settings &instance();

    explicit Settings(QWidget *parent = 0);
    ~Settings();

    params_t params() const;

public slots:
    void accept();
    void reject();

private slots:
    void update();
    void chooseBodyColor();
    void chooseHighlightColor();

private:
    Ui::Settings *ui;
    params_t m_params;
    params_t m_tmp_params;
};

#endif // SETTINGS_H
