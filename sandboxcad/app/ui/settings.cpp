#include "settings.h"
#include "ui_settingsdialog.h"

Settings &Settings::instance()
{
    static Settings m_instance;
    return m_instance;
}

Settings::Settings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);

    update();

    setFixedSize(size());

    QObject::connect(ui->cb_tolerance, static_cast<void (QDoubleSpinBox::*)(double d)>(&QDoubleSpinBox::valueChanged), this, [=](double d){ m_tmp_params.tolerance = d; });
    QObject::connect(ui->select_bodycolor, &QToolButton::clicked, this, &Settings::chooseBodyColor);
    QObject::connect(ui->select_highlightcolor, &QToolButton::clicked, this, &Settings::chooseHighlightColor);
    QObject::connect(ui->enable_highlight, &QCheckBox::stateChanged, this, [=](int state){ m_tmp_params.enable_highlight = (bool)state; });
}

Settings::~Settings()
{
    delete ui;
}

Settings::params_t Settings::params() const
{
    return m_params;
}

void Settings::accept()
{
    m_params = m_tmp_params;

    update();

    QDialog::accept();
}

void Settings::reject()
{
    m_tmp_params = m_params;

    update();

    QDialog::reject();
}

void Settings::update()
{
    ui->cb_tolerance->setValue(m_params.tolerance);
    ui->bodycolor->setText(m_params.body_color.name(QColor::HexRgb).toUpper());
    ui->highlightcolor->setText(m_params.highlight_color.name(QColor::HexRgb).toUpper());
    ui->enable_highlight->setChecked(m_params.enable_highlight);
}

void Settings::chooseBodyColor()
{
    QColor color = QColorDialog::getColor(m_tmp_params.body_color, this, "Цвет тел");

    if(color.isValid()) {
        m_tmp_params.body_color = color;
        ui->bodycolor->setText(color.name(QColor::HexRgb).toUpper());
    }
}

void Settings::chooseHighlightColor()
{
    QColor color = QColorDialog::getColor(m_tmp_params.highlight_color, this, "Цвет подсветки выделения");

    if(color.isValid()) {
        m_tmp_params.highlight_color = color;
        ui->highlightcolor->setText(color.name(QColor::HexRgb).toUpper());
    }
}
