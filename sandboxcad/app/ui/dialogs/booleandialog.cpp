#include "booleandialog.h"
#include "ui_booleandialog.h"

BooleanDialog::BooleanDialog(QString title, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BooleanDialog)
{
    ui->setupUi(this);    
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);

    setWindowTitle(title);
    setFixedSize(size());
    setAttribute(Qt::WA_DeleteOnClose, true);
    setWindowFlags(Qt::WindowTitleHint | Qt::WindowCloseButtonHint | Qt::WindowStaysOnTopHint);

    ui->buttonBox->button(QDialogButtonBox::Ok)->setText("ОК");
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setText("Отмена");

    QObject::connect(ui->tool_add, &QToolButton::clicked, this, [=]() {
        if(m_ct.s_tool == 0) return;
        if(m_ct.tools.contains(m_ct.s_tool)) return;
        if(m_ct.target == m_ct.s_tool) return;

        ui->tools_list->addItem(QString::number(m_ct.s_tool));
        m_ct.tools.append(m_ct.s_tool);

        m_ct.s_tool = 0;

        if(m_ct.target != 0 && !m_ct.tools.empty()) {
            ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
        }
    });

    QObject::connect(ui->target_select, &QPushButton::clicked, this, [=]() {
         ui->tool_select->setChecked(false);
    });

    QObject::connect(ui->tool_select, &QPushButton::clicked, this, [=]() {
        ui->target_select->setChecked(false);
    });
}

BooleanDialog::~BooleanDialog()
{
    delete ui;
}

BooleanDialog::controller_t BooleanDialog::controller() const
{
    return m_ct;
}

void BooleanDialog::select(PK_BODY_t t)
{
    if(ui->target_select->isChecked())
        m_ct.target = t;
    else if(ui->tool_select->isChecked())
        m_ct.s_tool = t;

    if(m_ct.target != 0 && !m_ct.tools.empty()) {
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
    }
}
