#include "conedialog.h"
#include "ui_conedialog.h"

ConeDialog::ConeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConeDialog)
{
    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose, true);
    setWindowFlags(Qt::WindowTitleHint | Qt::WindowCloseButtonHint | Qt::WindowStaysOnTopHint);

    ui->buttonBox->button(QDialogButtonBox::Ok)->setText("ОК");
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setText("Отмена");

    setFixedSize(size());
}

ConeDialog::~ConeDialog()
{
    delete ui;
}

QVector3D ConeDialog::origin_point()
{
    return QVector3D(ui->op_x->text().toFloat(),
                     ui->op_y->text().toFloat(),
                     ui->op_z->text().toFloat());
}

QVector3D ConeDialog::axis()
{
    return QMap<QString, QVector3D>
    {
        { "X", QVector3D(1, 0, 0) },
        { "Y", QVector3D(0, 1, 0) },
        { "Z", QVector3D(0, 0, 1) }
    }[ui->axis->currentText()];
}

QVector3D ConeDialog::ref_axis()
{
    if((axis().x() != 0) || (axis().z() != 0))
        return QVector3D(0, 1, 0);
    else return QVector3D(0, 0, 1);
}

float ConeDialog::lradius()
{
    return ui->lr->text().toFloat();
}

float ConeDialog::angle()
{
    return ui->angle->text().toFloat();
}

float ConeDialog::height()
{
    return ui->h->text().toFloat();
}
