#ifndef CONEDIALOG_H
#define CONEDIALOG_H

#include <QDialog>
#include <QVector3D>
#include <QPushButton>

namespace Ui {
class ConeDialog;
}

class ConeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ConeDialog(QWidget *parent = 0);
    ~ConeDialog();

    QVector3D origin_point();
    QVector3D axis();
    QVector3D ref_axis();

    float lradius();
    float angle();
    float height();

private:
    Ui::ConeDialog *ui;
};

#endif // CONEDIALOG_H
