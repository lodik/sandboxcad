#ifndef CYLINDERDIALOG_H
#define CYLINDERDIALOG_H

#include <QDialog>
#include <QVector3D>
#include <QPushButton>

namespace Ui {
class CylinderDialog;
}

class CylinderDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CylinderDialog(QWidget *parent = 0);
    ~CylinderDialog();

    QVector3D origin_point();
    QVector3D axis();
    QVector3D ref_axis();

    float radius();
    float height();

private:
    Ui::CylinderDialog *ui;
};

#endif // CYLINDERDIALOG_H
