#ifndef CUBEDIALOG_H
#define CUBEDIALOG_H

#include <QDialog>
#include <QVector3D>
#include <QPushButton>

namespace Ui {
class CubeDialog;
}

class CubeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CubeDialog(QWidget *parent = 0);
    ~CubeDialog();

    QVector3D origin_point();
    QVector3D axis();
    QVector3D ref_axis();

    QVector3D dimensions();

private:
    Ui::CubeDialog *ui;
};

#endif // CUBEDIALOG_H
