#ifndef MAPPING_H
#define MAPPING_H

#define map_primitive(pname, dname, __accepted_func)\
    QObject::connect(ui->action_cr_##pname, &QAction::toggled, this, [=](bool checked) {\
        if(checked) {\
            dialogs.pname = new dname##Dialog();\
            dialogs.pname->show();\
            \
            QObject::connect(dialogs.pname, &QDialog::accepted, this, [=]() {\
                PK_BODY_t body;\
                PK_AXIS2_sf_s ax;\
                ax.axis = { dialogs.pname->axis().x(), dialogs.pname->axis().y(), dialogs.pname->axis().z() };\
                ax.ref_direction = { dialogs.pname->ref_axis().x(), dialogs.pname->ref_axis().y(), dialogs.pname->ref_axis().z() };\
                ax.location = { dialogs.pname->origin_point().x(), dialogs.pname->origin_point().y(), dialogs.pname->origin_point().z() };\
                __accepted_func;\
                view->update();\
                emit ui->cb_filter->activated(ui->cb_filter->currentIndex());\
                \
                ui->action_cr_##pname->toggle();\
            });\
            \
            QObject::connect(dialogs.pname, &QDialog::rejected, this, [=]() {\
                ui->action_cr_##pname->toggle();\
            });\
        }\
        else {\
            dialogs.pname->disconnect();\
            dialogs.pname->close();\
        }\
    });\

#define map_boolean(bname, dtitle)\
    QObject::connect(ui->action_b_##bname, &QAction::toggled, this, [=](bool checked) {\
        if(checked) {\
            dialogs.bname = new BooleanDialog(dtitle);\
            dialogs.bname->show();\
            \
            QObject::connect(ui->list_elements, &QListWidget::itemActivated, this, [=](QListWidgetItem *item) {\
                dialogs.bname->select(item->data(Qt::UserRole).value<int>());\
            });\
            \
            QObject::connect(dialogs.bname, &QDialog::accepted, this, [=]() {\
                BooleanOperations::bname(dialogs.bname->controller().target, dialogs.bname->controller().tools);\
                \
                view->update();\
                emit ui->cb_filter->activated(ui->cb_filter->currentIndex());\
                \
                ui->action_b_##bname->toggle();\
            });\
            \
            QObject::connect(dialogs.bname, &QDialog::rejected, this, [=]() {\
                ui->action_b_##bname->toggle();\
            });\
        }\
        else {\
            ui->list_elements->disconnect(this);\
            \
            dialogs.bname->disconnect();\
            dialogs.bname->close();\
        }\
    });\

#endif // MAPPING_H

