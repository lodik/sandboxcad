#include "cubedialog.h"
#include "ui_cubedialog.h"

CubeDialog::CubeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CubeDialog)
{
    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose, true);
    setWindowFlags(Qt::WindowTitleHint | Qt::WindowCloseButtonHint | Qt::WindowStaysOnTopHint);

    ui->buttonBox->button(QDialogButtonBox::Ok)->setText("ОК");
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setText("Отмена");

    setFixedSize(size());
}

CubeDialog::~CubeDialog()
{
    delete ui;
}

QVector3D CubeDialog::origin_point()
{
    return QVector3D(ui->op_x->text().toFloat(),
                     ui->op_y->text().toFloat(),
                     ui->op_z->text().toFloat());
}

QVector3D CubeDialog::axis()
{
    return QMap<QString, QVector3D>
    {
        { "X", QVector3D(1, 0, 0) },
        { "Y", QVector3D(0, 1, 0) },
        { "Z", QVector3D(0, 0, 1) }
    }[ui->axis->currentText()];
}

QVector3D CubeDialog::ref_axis()
{
    if((axis().x() != 0) || (axis().z() != 0))
        return QVector3D(0, 1, 0);
    else return QVector3D(0, 0, 1);
}

QVector3D CubeDialog::dimensions()
{
    return QVector3D(ui->sz_l->text().toFloat(),
                     ui->sz_w->text().toFloat(),
                     ui->sz_h->text().toFloat());
}

