#include "cylinderdialog.h"
#include "ui_cylinderdialog.h"

CylinderDialog::CylinderDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CylinderDialog)
{
    ui->setupUi(this);

    setAttribute(Qt::WA_DeleteOnClose, true);
    setWindowFlags(Qt::WindowTitleHint | Qt::WindowCloseButtonHint | Qt::WindowStaysOnTopHint);

    ui->buttonBox->button(QDialogButtonBox::Ok)->setText("ОК");
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setText("Отмена");

    setFixedSize(size());
}

CylinderDialog::~CylinderDialog()
{
    delete ui;
}

QVector3D CylinderDialog::origin_point()
{
    return QVector3D(ui->op_x->text().toFloat(),
                     ui->op_y->text().toFloat(),
                     ui->op_z->text().toFloat());
}

QVector3D CylinderDialog::axis()
{
    return QMap<QString, QVector3D>
    {
        { "X", QVector3D(1, 0, 0) },
        { "Y", QVector3D(0, 1, 0) },
        { "Z", QVector3D(0, 0, 1) }
    }[ui->axis->currentText()];
}

QVector3D CylinderDialog::ref_axis()
{
    if((axis().x() != 0) || (axis().z() != 0))
        return QVector3D(0, 1, 0);
    else return QVector3D(0, 0, 1);
}

float CylinderDialog::radius()
{
    return ui->radius->text().toFloat();
}

float CylinderDialog::height()
{
    return ui->height->text().toFloat();
}
