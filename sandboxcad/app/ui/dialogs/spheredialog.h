#ifndef SPHEREDIALOG_H
#define SPHEREDIALOG_H

#include <QDialog>
#include <QVector3D>
#include <QPushButton>

namespace Ui {
class SphereDialog;
}

class SphereDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SphereDialog(QWidget *parent = 0);
    ~SphereDialog();

    QVector3D origin_point();
    QVector3D axis();
    QVector3D ref_axis();

    float radius();

private:
    Ui::SphereDialog *ui;
};

#endif // SPHEREDIALOG_H
