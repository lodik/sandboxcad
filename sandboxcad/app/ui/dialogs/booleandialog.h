#ifndef BOOLEANDIALOG_H
#define BOOLEANDIALOG_H

#include <QDialog>
#include <QVector>
#include <QPushButton>

#include "parasolid/operations/booleanoperations.h"

#include "easylogging++.h"

namespace Ui {
class BooleanDialog;
}

class BooleanDialog : public QDialog
{
    Q_OBJECT

private:
    struct controller_t {
        PK_BODY_t target = 0;
        PK_BODY_t s_tool = 0;
        QVector<PK_BODY_t> tools;
    };

public:
    explicit BooleanDialog(QString title, QWidget *parent = 0);
    ~BooleanDialog();

    controller_t controller() const;

public slots:
    void select(PK_BODY_t t);

private:
    Ui::BooleanDialog *ui;
    controller_t m_ct;
};

#endif // BOOLEANDIALOG_H
