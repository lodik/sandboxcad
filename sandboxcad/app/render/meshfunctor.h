#ifndef QMESHFUNCTOR_H
#define QMESHFUNCTOR_H

#include <Qt3DRenderer/QAbstractMeshFunctor>
#include <Qt3DRenderer/QMeshData>
#include <Qt3DRenderer/Attribute>
#include <Qt3DRenderer/Buffer>

class MeshFunctor : public Qt3D::QAbstractMeshFunctor
{
public:
    MeshFunctor(QVector<double> vertices, QVector<double> normals, QVector<double> colors);
    Qt3D::QMeshDataPtr operator()() override;
    bool operator==(const QAbstractMeshFunctor &other) const override;

private:
    QVector<double> m_vertices;
    QVector<double> m_normals;
    QVector<double> m_colors;
};

#endif // QMESHFUNCTOR_H
