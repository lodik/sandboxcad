#include "meshfunctor.h"

Qt3D::QMeshDataPtr createMesh(QVector<double> vertices, QVector<double> normals, QVector<double> colors);

MeshFunctor::MeshFunctor(QVector<double> vertices, QVector<double> normals, QVector<double> colors) :
    m_vertices(vertices),
    m_normals(normals),
    m_colors(colors)
{

}

Qt3D::QMeshDataPtr MeshFunctor::operator()()
{
    return createMesh(m_vertices, m_normals, m_colors);
}

bool MeshFunctor::operator==(const Qt3D::QAbstractMeshFunctor &other) const
{
    const MeshFunctor *otherfunctor = dynamic_cast<const MeshFunctor *>(&other);
    if(otherfunctor != nullptr)
        return (otherfunctor->m_vertices == m_vertices &&
                otherfunctor->m_normals == m_normals &&
                otherfunctor->m_colors == m_colors);

     return false;
}

Qt3D::QMeshDataPtr createMesh(QVector<double> vertices, QVector<double> normals, QVector<double> colors)
{
    const int size = 3 + 3 + 3;
    const int stride = size * sizeof(double);

    QByteArray vertex_bytes;
    vertex_bytes.resize(stride * vertices.size());

    double *bptr = reinterpret_cast<double *>(vertex_bytes.data());

    for(int i = 0, j = 0, k = 0; i < vertices.size();)
    {
        *bptr++ = vertices[i++];
        *bptr++ = vertices[i++];
        *bptr++ = vertices[i++];

        *bptr++ = normals[j++];
        *bptr++ = normals[j++];
        *bptr++ = normals[j++];

        *bptr++ = colors[k++];
        *bptr++ = colors[k++];
        *bptr++ = colors[k++];
    }

    Qt3D::BufferPtr vbuf(new Qt3D::Buffer(QOpenGLBuffer::VertexBuffer));
    vbuf->setUsage(QOpenGLBuffer::StaticDraw);
    vbuf->setData(vertex_bytes);

    Qt3D::QMeshDataPtr mesh(new Qt3D::QMeshData(Qt3D::QMeshData::Triangles));

    int offset = 0;

    mesh->addAttribute(Qt3D::QMeshData::defaultPositionAttributeName(),
                       Qt3D::AttributePtr(new Qt3D::Attribute(vbuf, GL_DOUBLE_VEC3, vertices.size(), offset, stride)));

    offset += 3 * sizeof(double);

    mesh->addAttribute(Qt3D::QMeshData::defaultNormalAttributeName(),
                       Qt3D::AttributePtr(new Qt3D::Attribute(vbuf, GL_DOUBLE_VEC3, normals.size(), offset, stride)));

    offset += 3 * sizeof(double);

    mesh->addAttribute(Qt3D::QMeshData::defaultColorAttributeName(),
                       Qt3D::AttributePtr(new Qt3D::Attribute(vbuf, GL_DOUBLE_VEC3, colors.size(), offset, stride)));


    const int indices = vertices.size() / 3;

    QByteArray index_bytes;
    index_bytes.resize(indices * sizeof(int));
    int *iptr = reinterpret_cast<int *>(index_bytes.data());

    for(int i = 0; i < indices; i++)
        *iptr++ = i;

    Qt3D::BufferPtr ibuf(new Qt3D::Buffer(QOpenGLBuffer::IndexBuffer));
    ibuf->setUsage(QOpenGLBuffer::StaticDraw);
    ibuf->setData(index_bytes);

    mesh->setIndexAttribute(Qt3D::AttributePtr(new Qt3D::Attribute(ibuf, GL_UNSIGNED_INT, indices, 0, 0)));

    mesh->computeBoundsFromAttribute(Qt3D::QMeshData::defaultPositionAttributeName());

    return mesh;
}
