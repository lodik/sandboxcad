#ifndef WIREFRAMEFUNCTOR_H
#define WIREFRAMEFUNCTOR_H

#include <Qt3DRenderer/QAbstractMeshFunctor>
#include <Qt3DRenderer/QMeshData>
#include <Qt3DRenderer/Attribute>
#include <Qt3DRenderer/Buffer>

class WireframeFunctor : public Qt3D::QAbstractMeshFunctor
{
public:
    WireframeFunctor(QVector<double> vertices, QVector<double> colors);
    Qt3D::QMeshDataPtr operator()() override;
    bool operator==(const QAbstractMeshFunctor &other) const override;

private:
    QVector<double> m_vertices;
    QVector<double> m_colors;
};

#endif // WIREFRAMEFUNCTOR_H
