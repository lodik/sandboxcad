#include "mesh.h"

Mesh::Mesh(Type type, QNode *parent) :
    QAbstractMesh(parent),
    m_type(type)
{
    Qt3D::QAbstractMesh::update();
}

void Mesh::setVertices(QVector<double> vertices)
{
    m_vertices = vertices;
    Qt3D::QAbstractMesh::update();
}

void Mesh::setNormals(QVector<double> normals)
{
    m_normals = normals;
    Qt3D::QAbstractMesh::update();
}

void Mesh::setColors(QVector<double> colors)
{
    m_colors = colors;
    Qt3D::QAbstractMesh::update();
}

Qt3D::QAbstractMeshFunctorPtr Mesh::meshFunctor() const
{
    if(m_type == Type::MESH)
        return Qt3D::QAbstractMeshFunctorPtr(new MeshFunctor(m_vertices, m_normals, m_colors));
    else if(m_type == Type::WIREFRAME)
        return Qt3D::QAbstractMeshFunctorPtr(new WireframeFunctor(m_vertices, m_colors));
}

void Mesh::copy(const Qt3D::QNode *ref)
{
    QAbstractMesh::copy(ref);

    const Mesh *mesh = static_cast<const Mesh*>(ref);

    m_vertices = mesh->m_vertices;
    m_normals = mesh->m_normals;
    m_colors = mesh->m_colors;
    m_type = mesh->m_type;
}
