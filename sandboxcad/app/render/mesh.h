#ifndef QMESH_H
#define QMESH_H

#include <Qt3DRenderer/QAbstractMesh>

#include <QSharedPointer>
#include <QByteArray>

#include "meshfunctor.h"
#include "wireframefunctor.h"

class Mesh : public Qt3D::QAbstractMesh
{
    Q_OBJECT

public:    
    enum class Type
    {
        MESH,
        WIREFRAME
    };

    explicit Mesh(Type type = Type::MESH, QNode *parent = 0);

    void setVertices(QVector<double> vertices);
    void setNormals(QVector<double> normals);
    void setColors(QVector<double> colors);

    Qt3D::QAbstractMeshFunctorPtr meshFunctor() const override;

protected:
    void copy(const QNode *ref) override;

private:
    QVector<double> m_vertices;
    QVector<double> m_normals;
    QVector<double> m_colors;

    Type m_type;

    QT3D_CLONEABLE(Mesh)
};

#endif // QMESH_H
