TEMPLATE = app
TARGET = SandboxCAD

QT += core gui widgets concurrent 3dcore 3drenderer 3dinput

BUILD_DIR = ../build/sandboxcad
DESTDIR = ../bin

CONFIG(release, debug|release) {
    win32:RC_FILE = scad.rc
}

include(../common.pri)

include(parasolid/parasolid.pri)
include(app/app.pri)

LIBS += -L../build/lib \
        -L$(PARASOLID_DIR)/lib \
        -lquazip \
        -lpskernel

DEPENDPATH += \
        $(PARASOLID_DIR)/include \
        ../libraries/quazip \
        ../libraries/easylogging

INCLUDEPATH += $$DEPENDPATH

RESOURCES += \
        res/ui/ui.qrc

SOURCES += \
        main.cpp

