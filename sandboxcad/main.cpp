#include <QApplication>

#include "easylogging++.h"

#include "parasolid/psession.h"

#include "app/ui/mainwindow.h"

INITIALIZE_EASYLOGGINGPP

int main(int argc, char *argv[]) {
    START_EASYLOGGINGPP(argc, argv);
    el::Loggers::reconfigureAllLoggers(el::ConfigurationType::Format, "[%level][%datetime{%H:%m:%s}][%func] %msg");

    QStringList paths = QCoreApplication::libraryPaths();
    paths.append(".");
    paths.append("platforms");
    QCoreApplication::setLibraryPaths(paths);

    LOG(INFO) << "Application started";
    QApplication a(argc, argv);

    pk::PSession::Instance().createSession();

    MainWindow w;
    w.show();

    // фикс бага с вылезающей за пределы лейаута вьюхой
    w.resize(w.width() + 1, w.height());

    return a.exec();
}

