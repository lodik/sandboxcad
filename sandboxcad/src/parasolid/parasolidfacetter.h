#ifndef PARASOLIDFACETTER_H
#define PARASOLIDFACETTER_H

#include "parasolid_kernel.h"
#include <QMap>
#include <QVector3DArray>

namespace pk {

namespace build {
class AbstractParasolidBody;
}


class ParasolidFacetter {
public:
    static void recalculateModelsData();
    static void recalculateFacets();
    static void recalculateGeometry();
private:
    static QVector3DArray getCurvePoints(PK_CURVE_t curve, PK_VECTOR_t from, PK_VECTOR_t to, int samples = 2);
    static QVector3DArray getPeriodicCurvePoints(PK_CURVE_t curve, int samples = 32);
    static QMap<int, QVector3DArray> &getEdgesData(pk::build::AbstractParasolidBody &body);
    static QMap<int, QVector3DArray> &getFinData(pk::build::AbstractParasolidBody &body);
    static void setDefaultOutputOptions(PK_TOPOL_facet_2_o_t *options);

};
}


#endif // PARASOLIDFACETTER_H
