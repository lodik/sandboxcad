#include "parasolidfacetter.h"
#include "parasolidbodymanager.h"
#include "abstractparasolidbody.h"
#include "easylogging++.h"

#include <QVector>
#include <QPair>
#include <QVector3DArray>

using namespace pk;
using namespace pk::build;

void ParasolidFacetter::recalculateModelsData()
{
    recalculateFacets();
    recalculateGeometry();
}

void ParasolidFacetter::recalculateFacets()
{
    PK_TOPOL_facet_2_o_t options;
    PK_TOPOL_facet_2_o_m(options);
    options.choice.facet_fin = PK_LOGICAL_true;
    options.choice.data_normal_idx = PK_LOGICAL_true;
    options.choice.normal_vec = PK_LOGICAL_true;
    options.choice.data_normal_idx = PK_LOGICAL_true;
    options.choice.fin_data = PK_LOGICAL_true;
    options.choice.point_vec = PK_LOGICAL_true;
    options.choice.data_point_idx = PK_LOGICAL_true;
    options.choice.fin_edge = PK_LOGICAL_true;

    setDefaultOutputOptions(&options);

    PK_TOPOL_fctab_facet_fin_s* facetFin;
    PK_TOPOL_fctab_fin_data_s* finData;
    PK_TOPOL_fctab_data_point_s* dataPointIdX;
    PK_TOPOL_fctab_data_normal_s* dataNormalIdX;
    PK_TOPOL_fctab_point_vec_s* pointVec;
    PK_TOPOL_fctab_normal_vec_s* normalVec;

    auto iterator = ParasolidBodyManager::instance().bodies();
    while (iterator.hasNext()) {
        AbstractParasolidBody& body = iterator.next();
        body.clearModelInformation();

        int topolCount;
        PK_TOPOL_t* topols;
        PK_BODY_ask_faces(body.bodyId(), &topolCount, &topols);
        PK_TOPOL_facet_2_r_t output;
        PK_TOPOL_facet_2(topolCount, topols, NULL, &options, &output);
        PK_MEMORY_free(topols);

        for (int i = 0; i < output.number_of_tables; i++) {
            switch (output.tables[i].fctab) {
            case PK_TOPOL_fctab_facet_fin_c:
                facetFin = output.tables[i].table.facet_fin;
                break;
            case PK_TOPOL_fctab_fin_data_c:
                finData = output.tables[i].table.fin_data;
                break;
            case PK_TOPOL_fctab_data_point_c:
                dataPointIdX = output.tables[i].table.data_point_idx;
                break;
            case PK_TOPOL_fctab_data_normal_c:
                dataNormalIdX = output.tables[i].table.data_normal_idx;
                break;
            case PK_TOPOL_fctab_point_vec_c:
                pointVec = output.tables[i].table.point_vec;
                break;
            case PK_TOPOL_fctab_normal_vec_c:
                normalVec = output.tables[i].table.normal_vec;
                break;
            default:
                break;
            }
        }

        QMultiMap<int, int> wFacetFins;
        for (int i = 0; i < facetFin->length; i++) {
            wFacetFins.insert(facetFin->data[i].facet, facetFin->data[i].fin);
        }
        QVector3DArray vertexes;
        QVector3DArray normals;

        for (int facet : wFacetFins.keys().toSet()) {
            for (int fin : wFacetFins.values(facet)) {
                int finIndex = finData->data[fin];
                int pointIndex = dataPointIdX->point[finIndex];
                int normalIndex = dataNormalIdX->normal[finIndex];
                double* vecCoords = pointVec->vec[pointIndex].coord;
                double* vecNormals = normalVec->vec[normalIndex].coord;
                vertexes.append(vecCoords[0], vecCoords[1], vecCoords[2]);
                normals.append(vecNormals[0], vecNormals[1], vecNormals[2]);
            }
        }
        LOG(INFO) << "Body{" << body.bodyId() << "} vertex count = " << vertexes.size() / 3 << "; normals count = " << normals.size() / 3;
        body.setVertexes(qMove(vertexes));
        body.setNormals(qMove(normals));
    }
}

void ParasolidFacetter::recalculateGeometry()
{
    auto iterator = ParasolidBodyManager::instance().bodies();
    while (iterator.hasNext()) {
        AbstractParasolidBody& body = iterator.next();
        LOG(INFO) << "Recalculating geometry for body {" << body.bodyId() << "}";
//        ParasolidBodyManager::instance().m_edges.remove(body.bodyId());
//        ParasolidBodyManager::instance().m_edges.insert(body.bodyId(), qMove(getEdgesData(body.bodyId())));
        ParasolidBodyManager::instance().m_fins.remove(body.bodyId());
        ParasolidBodyManager::instance().m_fins.insert(body.bodyId(), qMove(getFinData(body)));
    }
}

QMap<int, QVector3DArray>& ParasolidFacetter::getEdgesData(AbstractParasolidBody &body)
{
    QVector<PK_EDGE_t> allBodiesEdges = ParasolidBodyManager::instance().getAllBodiesEdges(body);
    QMap<int, QVector3DArray> data;
    for (auto edge : allBodiesEdges) {
        PK_CURVE_t curve;
        PK_EDGE_ask_curve(edge, &curve);
        PK_VERTEX_t vertexes[2];
        PK_EDGE_ask_vertices(edge, vertexes);
        QVector3DArray curvePoints;
        PK_PARAM_sf_s curve_param;
        PK_CURVE_ask_param(curve, &curve_param);
        if (curve_param.periodic == PK_PARAM_periodic_no_c || (vertexes[0] != PK_ENTITY_null && vertexes[1] != PK_ENTITY_null)) {
            PK_POINT_t p1, p2;
            PK_VERTEX_ask_point(vertexes[0], &p1);
            PK_VERTEX_ask_point(vertexes[1], &p2);
            PK_POINT_sf_s p_sf1;
            PK_POINT_ask(p1, &p_sf1);
            PK_POINT_sf_s p_sf2;
            PK_POINT_ask(p2, &p_sf2);
            curvePoints = getCurvePoints(curve, p_sf1.position, p_sf2.position, 4);
        }
        else {
            curvePoints = getPeriodicCurvePoints(curve);
        }
        data.insert(edge, qMove(curvePoints));
    }
    LOG(INFO) << "Edge count:" << data.keys().length();
    return qMove(data);
}

QMap<int, QVector3DArray>& ParasolidFacetter::getFinData(AbstractParasolidBody &body)
{
    QVector<PK_EDGE_t> allBodiesFins = ParasolidBodyManager::instance().getAllBodiesFins(body);
    QMap<int, QVector3DArray> data;
    for (auto fin : allBodiesFins) {
        PK_CURVE_t curve;
        PK_CLASS_t curve_class;
        PK_VECTOR_t positions[2];
        PK_INTERVAL_t curve_interval;
        PK_LOGICAL_t sense;
        PK_FIN_ask_geometry(fin, PK_LOGICAL_true, &curve, &curve_class, positions, &curve_interval, &sense);
        PK_LOGICAL_t isEqual;
        PK_VECTOR_is_equal(positions[0], positions[1], &isEqual);
        QVector3DArray curvePoints;
        if (isEqual == PK_LOGICAL_true) {
            curvePoints = getPeriodicCurvePoints(curve);
        } else {
            curvePoints = getCurvePoints(curve, positions[0], positions[1], 32);
        }
        data.insert(fin, curvePoints);
    }

    LOG(INFO) << "Fins count:" << data.keys().length();
    return qMove(data);
}

QVector3DArray ParasolidFacetter::getCurvePoints(PK_CURVE_t curve, PK_VECTOR_t from, PK_VECTOR_t to, int samples)
{
    PK_INTERVAL_t totalInterval;
    PK_INTERVAL_t interval;
    PK_CURVE_ask_interval(curve, &totalInterval);
    PK_CURVE_find_vector_interval(curve, from, to, &interval);
    LOG(TRACE) << "Curve " << curve << " total intervals: " << totalInterval.value[0] << " to " << totalInterval.value[1] << "; interval " << interval.value[0] << " to " << interval.value[1];
    double totalLenth = interval.value[1] - interval.value[0];
    double delta = totalLenth / samples;
    QVector3DArray points;
    for (double i = interval.value[0]; i <= interval.value[1]; i += delta) {
        PK_VECTOR_t coords;
        PK_CURVE_eval(curve, i, 0, &coords);
        points.append(coords.coord[0], coords.coord[1], coords.coord[2]);
    }
    return points;
}

QVector3DArray ParasolidFacetter::getPeriodicCurvePoints(PK_CURVE_t curve, int samples)
{
    QVector3DArray points;
    PK_INTERVAL_t totalInterval;
    PK_CURVE_ask_interval(curve, &totalInterval);
    LOG(TRACE) << "Curve " << curve << " total intervals: " << totalInterval.value[0] << " to " << totalInterval.value[1];
    double totalLenth = totalInterval.value[1] - totalInterval.value[0];
    double delta = totalLenth / samples;
    for (double i = totalInterval.value[0]; i <= totalInterval.value[1]; i += delta) {
        PK_VECTOR_t coords;
        PK_CURVE_eval(curve, i, 0, &coords);
        points.append(coords.coord[0], coords.coord[1], coords.coord[2]);
    }
    return points;
}

void ParasolidFacetter::setDefaultOutputOptions(PK_TOPOL_facet_2_o_t* options)
{
    options->control.quality = PK_facet_quality_improved_c;
    options->control.vertices_on_planar = PK_LOGICAL_true;
    options->control.match = PK_facet_match_topol_c;
    options->control.max_facet_sides = 3;
    options->control.is_surface_plane_tol = PK_LOGICAL_true;
    options->control.surface_plane_tol = 0.1;
    //options->control.is_max_facet_width = PK_LOGICAL_true;
    //options->control.max_facet_width = 0.5;
}
