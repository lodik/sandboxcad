HEADERS += \
        # parasolid headers
        parasolid/parasolidbodymanager.h \
        parasolid/parasolidfacetter.h \
        parasolid/parasolidfrustrum.h \
        parasolid/parasolidsession.h \
        parasolid/bodies/parasolidsolidcube.h \
        parasolid/bodies/parasolidsolidsphere.h \
        parasolid/bodies/parasolidsolidcylinder.h \
        parasolid/bodies/parasolidsolidcone.h \
        parasolid/bodies/abstractparasolidblockbuilder.h \
        parasolid/bodies/parasolidsolidblocks.h \
        parasolid/abstractparasolidbody.h \
        parasolid/parasolidbooleans.h


SOURCES += \
        parasolid/parasolidbodymanager.cpp \
        parasolid/parasolidfacetter.cpp \
        parasolid/parasolidfrustrum_win32.cpp \
        parasolid/parasolidfrustrumdelta.cpp \
        parasolid/parasolidsession.cpp \
        parasolid/bodies/parasolidsolidcube.cpp \
        parasolid/bodies/parasolidsolidsphere.cpp \
        parasolid/bodies/parasolidsolidcylinder.cpp \
        parasolid/bodies/parasolidsolidcone.cpp \
        parasolid/bodies/parasolidsolidblocks.cpp \
        parasolid/parasolidbooleans.cpp \
        parasolid/abstractparasolidbody.cpp
