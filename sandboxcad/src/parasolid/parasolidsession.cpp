#include "parasolidsession.h"

#include "parasolid_kernel.h"
#include "frustrum_ifails.h"

#include <assert.h>

#include "easylogging++.h"
#include "parasolidfrustrum.h"

using namespace pk;
using namespace pk::frustrum;

ParasolidSession::ParasolidSession()
{
}

ParasolidSession& ParasolidSession::Instance()
{
    static ParasolidSession m_instance;
    return m_instance;
}

bool ParasolidSession::createSession()
{
    bool ok = true;

    // Register frustrum functions
    // Note: the GO functions are registered in CExampleAppDoc

    PK_SESSION_frustrum_t fru;
    PK_SESSION_frustrum_o_m(fru);

    fru.fstart = startFrustrum;
    fru.fabort = abortFrustrum;
    fru.fstop = stopFrustrum;
    fru.fmallo = getMemory;
    fru.fmfree = returnMemory;
    fru.ffoprd = openReadFrustrumFile;
    fru.ffopwr = openWriteFrustrumFile;
    fru.ffclos = closeFrustrumFile;
    fru.ffread = readFromFrustrumFile;
    fru.ffwrit = writeToFrustrumFile;

    PK_SESSION_register_frustrum(&fru);
    // Register Delta Frustrum

    PK_DELTA_frustrum_t delta_fru;

    delta_fru.open_for_write_fn = FRU_delta_open_for_write;
    delta_fru.open_for_read_fn = FRU_delta_open_for_read;
    delta_fru.close_fn = FRU_delta_close;
    delta_fru.write_fn = FRU_delta_write;
    delta_fru.read_fn = FRU_delta_read;
    delta_fru.delete_fn = FRU_delta_delete;

    assert(PK_DELTA_register_callbacks(delta_fru) == PK_ERROR_no_errors);

    // Register Error Handler
    PK_ERROR_frustrum_t errorFru;
    errorFru.handler_fn = PKerrorHandler;
    assert(PK_ERROR_register_callbacks(errorFru) == PK_ERROR_no_errors);

    // Starts the modeller

    PK_SESSION_start_o_t options;
    PK_SESSION_start_o_m(options);

    PK_SESSION_start(&options);

    // Check to see if it all started up OK
    PK_LOGICAL_t was_error = PK_LOGICAL_true;
    PK_ERROR_sf_t error_sf;
    PK_ERROR_ask_last(&was_error, &error_sf);
    if (was_error)
        return false;

    int partitionsCount;
    PK_PARTITION_t* partitions;
    PK_SESSION_ask_partitions(&partitionsCount, &partitions);
    m_partitions.insert("default", partitions[0]);

    return ok;
}

bool ParasolidSession::createNewPartition(QString name)
{
    PK_PARTITION_t partition;
    PK_PARTITION_create_empty(&partition);
    if (m_partitions.find(name) != m_partitions.end()) {
        return false;
        LOG(WARNING) << "New partition creation fault!";
    }
    m_partitions.insert(name, partition);
    return true;
}

PK_PARTITION_t ParasolidSession::currentPartition()
{
    PK_PARTITION_t _currentPartition;
    PK_SESSION_ask_curr_partition(&_currentPartition);
    return _currentPartition;
}

void ParasolidSession::setPartition(PK_PARTITION_t partition)
{
    PK_PARTITION_set_current(partition);
    LOG(INFO) << "Partition changed to \"" << getPartitionName(partition).toStdString() << "\"";
}

PK_PARTITION_t ParasolidSession::getPartition(QString name)
{
    auto it = m_partitions.find(name);
    if (it == m_partitions.end())
        return -1;
    return *it;
}

QString ParasolidSession::getPartitionName(PK_PARTITION_t partition)
{
    foreach (QString partitionName, m_partitions.keys()) {
        if (partition == m_partitions[partitionName]) {
            return partitionName;
        }
    }
}

QMap<QString, PK_PARTITION_t> ParasolidSession::getPartitions()
{
    return m_partitions;
}

bool ParasolidSession::stopSession()
{
    PK_SESSION_stop();
    return true;
}

void ParasolidSession::startFrustrum(int* ifail)
{
    *ifail = FR_no_errors;
    FRU__delta_init(1);
    startFileFrustrum(ifail);
}

void ParasolidSession::stopFrustrum(int* ifail)
{

    *ifail = FR_no_errors;
    FRU__delta_init(2);
    stopFileFrustrum(ifail);
}

void ParasolidSession::getMemory(int* nBytes, char** memory, int* ifail)
{

    *memory = new char[*nBytes];
    *ifail = (*memory) ? FR_no_errors : FR_memory_full;
}

void ParasolidSession::returnMemory(int* nBytes, char** memory, int* ifail)
{
    delete[] * memory;
    *ifail = FR_no_errors;
}

PK_ERROR_code_t ParasolidSession::PKerrorHandler(PK_ERROR_sf_t* error)
{
    LOG(WARNING) << "PK error: " << error->function << " returned "
                 << error->code_token << ".";
    return error->code;
}
