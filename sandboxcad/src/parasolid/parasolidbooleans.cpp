#include "parasolidbooleans.h"

using namespace pk::build;
using namespace pk::operations;

void ParasolidBooleans::Unite(AbstractParasolidBody& target, AbstractParasolidBody& tool)
{
    PK_BODY_boolean_o_t bool_opts;
    PK_TOPOL_track_r_t tracking;
    PK_boolean_r_t results;
    PK_BODY_boolean_o_m(bool_opts);
    bool_opts.function = PK_boolean_unite_c;
    auto toolsArray = new PK_BODY_t[]{ tool.bodyId() };
    PK_BODY_boolean_2(target.bodyId(), 1, toolsArray, &bool_opts, &tracking, &results);

    //delete toolsArray;
    ParasolidBodyManager::instance().removeBody(target);
    ParasolidBodyManager::instance().removeBody(tool);
    AbstractParasolidBody(results.bodies[0]);
}

void ParasolidBooleans::Subtract(AbstractParasolidBody& target, AbstractParasolidBody& tool)
{
    PK_BODY_boolean_o_t bool_opts;
    PK_TOPOL_track_r_t tracking;
    PK_boolean_r_t results;
    PK_BODY_boolean_o_m(bool_opts);
    bool_opts.function = PK_boolean_subtract_c;
    auto toolsArray = new PK_BODY_t[]{ tool.bodyId() };
    PK_BODY_boolean_2(target.bodyId(), 1, toolsArray, &bool_opts, &tracking, &results);

    //delete toolsArray;
    ParasolidBodyManager::instance().removeBody(target);
    ParasolidBodyManager::instance().removeBody(tool);
    AbstractParasolidBody(results.bodies[0]);
}
