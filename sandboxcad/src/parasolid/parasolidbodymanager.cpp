#include "parasolidbodymanager.h"
#include "easylogging++.h"

#include "parasolid_kernel.h"
#include "parasolidsession.h"
#include "parasolidfacetter.h"
#include "abstractparasolidbody.h"

#include <QVector>
#include <QVector3DArray>

using namespace pk;
using namespace pk::build;

ParasolidBodyManager::ParasolidBodyManager()
{
}

ParasolidBodyManager& ParasolidBodyManager::instance()
{
    static ParasolidBodyManager m_instance;
    return m_instance;
}

void ParasolidBodyManager::update()
{
    m_edges.clear();
    m_fins.clear();
    ParasolidFacetter::recalculateModelsData();
}

QMutableVectorIterator<AbstractParasolidBody> ParasolidBodyManager::bodies()
{
    return QMutableVectorIterator<AbstractParasolidBody>(m_bodys);
}

QMapIterator<int, QMap<int, QVector3DArray> > ParasolidBodyManager::edges()
{
    return QMapIterator<int, QMap<int, QVector3DArray>>(m_edges);
}

QMapIterator<int, QMap<int, QVector3DArray>> ParasolidBodyManager::fins()
{
    return QMapIterator<int, QMap<int, QVector3DArray>>(m_fins);
}

void ParasolidBodyManager::addBody(AbstractParasolidBody& body)
{
    m_bodys.append(qMove(body));
}

void ParasolidBodyManager::removeBody(AbstractParasolidBody& body)
{
    clearBodyData(body.bodyId());
    m_bodys.removeAt(m_bodys.indexOf(body));
}

void ParasolidBodyManager::addBodyNormals(int id, QVector3DArray& normals)
{
    m_bodyNormals.insert(id, qMove(normals));
}

QVector3DArray& ParasolidBodyManager::getBodyData(int id)
{
    return m_bodyData[id];
}

QVector3DArray& ParasolidBodyManager::getBodyNormals(int id)
{
    return m_bodyNormals[id];
}

void ParasolidBodyManager::addBodyData(int id, QVector3DArray& data)
{
    m_bodyData.insert(id, qMove(data));
}

void ParasolidBodyManager::removeBodyData(int id)
{
    m_bodyData.remove(id);
}

void ParasolidBodyManager::removeBodyNormals(int id)
{
    m_bodyNormals.remove(id);
}

void ParasolidBodyManager::clearBodyData(int id)
{
    removeBodyData(id);
    removeBodyNormals(id);
}

QVector<PK_EDGE_t> ParasolidBodyManager::getAllBodiesEdges(AbstractParasolidBody& body)
{
    QVector<PK_EDGE_t> _edges;
    int nEdges;
    PK_EDGE_t* edges;
    PK_BODY_ask_edges(body.bodyId(), &nEdges, &edges);
    LOG(INFO) << "Body{" << body.bodyId() << "} have " << nEdges << " edges";
    for (int i = 0; i < nEdges; i++) {
        _edges.append(edges[i]);
    }
    return _edges;
}

QVector<PK_FIN_t> ParasolidBodyManager::getAllBodiesFins(AbstractParasolidBody &body)
{
    QVector<PK_FIN_t> _fins;
    int nFins;
    PK_FIN_t* fins;
    PK_BODY_ask_fins(body.bodyId(), &nFins, &fins);
    LOG(INFO) << "Body{" << body.bodyId() << "} have " << nFins << " fins";
    for (int i = 0; i < nFins; i++) {
        _fins.append(fins[i]);
    }
    return _fins;
}
