#include "abstractparasolidbody.h"
#include "easylogging++.h"

using namespace pk::build;

AbstractParasolidBody::AbstractParasolidBody()
    : m_body(0)
{
}

AbstractParasolidBody::AbstractParasolidBody(PK_BODY_t body)
    : m_body(body)
{
    ParasolidBodyManager::instance().addBody(*this);
}

AbstractParasolidBody::~AbstractParasolidBody()
{
}

PK_BODY_t AbstractParasolidBody::bodyId() const
{
    return m_body;
}

void AbstractParasolidBody::clearModelInformation()
{
    ParasolidBodyManager::instance().clearBodyData(this->bodyId());
}

QVector3DArray& AbstractParasolidBody::getVertexes() const
{
    return ParasolidBodyManager::instance().getBodyData(this->bodyId());
}

void AbstractParasolidBody::setVertexes(QVector3DArray& value)
{
    ParasolidBodyManager::instance().addBodyData(this->bodyId(), qMove(value));
}

QVector3DArray& AbstractParasolidBody::getNormals() const
{
    return ParasolidBodyManager::instance().getBodyNormals(this->bodyId());
}

void AbstractParasolidBody::setNormals(QVector3DArray& value)
{
    ParasolidBodyManager::instance().addBodyNormals(this->bodyId(), qMove(value));
}
