#ifndef PARASOLIDBODYMANAGER_H
#define PARASOLIDBODYMANAGER_H

#include <QVector3D>
#include <QVector3DArray>

#include "parasolid_kernel.h"
#include "parasolidfacetter.h"

namespace pk {

namespace build {
    class AbstractParasolidBody;
}

class ParasolidBodyManager {
  public:
    friend class ParasolidFacetter;

    static ParasolidBodyManager &instance();

    void update();
    QMutableVectorIterator<build::AbstractParasolidBody> bodies();
    QMapIterator<int, QMap<int, QVector3DArray>> edges();
    QMapIterator<int, QMap<int, QVector3DArray>> fins();
    void addBody(build::AbstractParasolidBody &body);
    void removeBody(build::AbstractParasolidBody &body);

    void addBodyData(int id, QVector3DArray &data);
    void addBodyNormals(int id, QVector3DArray &normals);
    QVector3DArray &getBodyData(int id);
    QVector3DArray &getBodyNormals(int id);
    void removeBodyData(int id);
    void removeBodyNormals(int id);
    void clearBodyData(int id);
    QVector<PK_EDGE_t> getAllBodiesEdges(build::AbstractParasolidBody &body);
    QVector<PK_FIN_t> getAllBodiesFins(build::AbstractParasolidBody &body);

private:
    ParasolidBodyManager();
    QVector<build::AbstractParasolidBody> m_bodys;
    QMap<int, QMap<int, QVector3DArray>> m_edges;
    QMap<int, QMap<int, QVector3DArray>> m_fins;
    QMap<int, QVector3DArray> m_bodyData;
    QMap<int, QVector3DArray> m_bodyNormals;

};
}


#endif // PARASOLIDBODYMANAGER_H
