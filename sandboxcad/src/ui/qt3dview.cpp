#include "qt3dview.h"

Qt3DView::Qt3DView(QSurfaceFormat format)
    : QGLView(format)
{
    this->setOption(QGLView::FOVZoom, true);
    this->setOption(QGLView::ObjectPicking, true);

    current_scene = new Qt3DScene(this);
    setScene(current_scene);

    camera()->setProjectionType(QGLCamera::Orthographic);

    camera()->setViewSize(QSizeF(25, 25));
    camera()->setEye(QVector3D(0, 0, 50));
}

Qt3DView::~Qt3DView()
{
    delete current_scene;
}

Qt3DScene *Qt3DView::scene() const
{
    return current_scene;
}

void Qt3DView::setScene(Qt3DScene *s)
{
    current_scene->disconnect(this);
    deregisterPickableNodes();

    current_scene = s;

    QObject::connect(current_scene, &Qt3DScene::sceneUpdated, this, &Qt3DView::update);
    QObject::connect(current_scene, &Qt3DScene::sceneUpdated, this, &Qt3DView::registerPickableNodes);

    emit current_scene->sceneUpdated();
}

void Qt3DView::registerPickableNodes()
{
    current_scene->generatePickNodes();

    for(QGLPickNode *pn : current_scene->pickNodes())
    {
        pn->disconnect(this);
        QObject::connect(pn, &QGLPickNode::clicked, this, [=]() { current_scene->select(pn->target()); }); // todo: just todo

        registerObject(pn->id(), pn);
    }
}

void Qt3DView::deregisterPickableNodes()
{
    for(QGLPickNode *pn : current_scene->pickNodes())
    {
        deregisterObject(pn->id());
    }
}

void Qt3DView::initializeGL(QGLPainter *painter)
{
    painter->setStandardEffect(QGL::LitMaterial);
    //painter->setColor(QColor(170, 200, 0));
    //painter->setFaceColor(QGL::AllFaces, QColor(170, 200, 0));

    /*auto light = new QGLLightParameters(this);
    light->setAmbientColor(Qt::white);
    light->setDiffuseColor(Qt::white);
    light->setDirection(QVector3D(0.0f, 0.0f, 1.0f));

    painter->setMainLight(light);*/

    auto model = new QGLLightModel(this);
    model->setAmbientSceneColor(Qt::white);
    model->setColorControl(QGLLightModel::SingleColor);
    model->setModel(QGLLightModel::OneSided);
    model->setViewerPosition(QGLLightModel::ViewerAtInfinity);

    painter->setLightModel(model);
}

void Qt3DView::earlyPaintGL(QGLPainter *painter)
{
    painter->setClearColor(QColor(25, 25, 25));
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Qt3DView::paintGL(QGLPainter *painter)
{
    current_scene->mainNode()->draw(painter);

    // все не работает
    auto fins = pk::ParasolidBodyManager::instance().fins();
    while(fins.hasNext()) {
        QMap<int, QVector3DArray> bodyFins = fins.next().value();
//        for (int key : bodyFins.keys()) {
//            QVector3DArray findata = bodyFins[key];
//            QGeometryData gd;
//            gd.appendVertexArray(findata);
//            gd.draw(painter, 0, findata.count(), QGL::LineStrip, 10.0f);

//            painter->setColor(QColor(1, 0, 0));
//            painter->setVertexAttribute(QGL::Position, findata);
//            painter->draw(QGL::LineStrip, findata.count());
//        }
    }
//    auto curves = pk::ParasolidBodyManager::instance().fins();
//    while(curves.hasNext())
//    {
//        curves.next();
//        for (auto fin : curves.value().values()) {
//            QGeometryData gd;
//            gd.appendVertexArray(fin);
//            gd.draw(painter, 0, fin.count(), QGL::LineStrip, 10.0f);

//            painter->setColor(QColor(0, 0, 0));
//            painter->setVertexAttribute(QGL::Position, fin);
//            painter->draw(QGL::LineStrip, fin.count());
//        }
//    }
}
