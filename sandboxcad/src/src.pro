TEMPLATE = app
TARGET = SandboxCAD

QT += core gui widgets 3d

# build destination config
BUILD_DIR = ../../build/sandboxcad
include(../../common.pri)
include(parasolid/parasolid.pri)

DESTDIR = ../../bin

# libs
LIBS += -L../../build/lib \
        -L$(PARASOLID_DIR)/lib \
        -lquazip \
        -lpskernel \

# dependencies
DEPENDPATH += \
        $(PARASOLID_DIR)/include \
        ../../libraries/quazip \
        ../../libraries/easylogging \

INCLUDEPATH += $$DEPENDPATH

# resources
RESOURCES += \
        ../res/ui/ui.qrc

# headers
HEADERS += \
        # ui logic
        ui/mainwindow.h \
        ui/qt3dview.h \
        ui/qt3dscene.h \
        # forms
        ui/forms/baseform.h \
        ui/forms/formcontroller.h \
        ui/forms/formui.h \
        ui/forms/uniteform.h \
        ui/forms/subtractform.h \
        # file io
        fileio/sxdoc.h

# sources
SOURCES += \
        # core
        main.cpp \
        # ui logic
        ui/mainwindow.cpp \
        ui/qt3dview.cpp \
        ui/qt3dscene.cpp \
        # forms
        ui/forms/baseform.cpp \
        ui/forms/formcontroller.cpp \
        ui/forms/formui.cpp \
        ui/forms/uniteform.cpp \
        ui/forms/subtractform.cpp \
        # file io
        fileio/sxdoc.cpp

# forms
FORMS += \
        ui/forms/mainwindow.ui
