#include "constractiveoperations.h"

#include "easylogging++.h"

#include "parasolid/entitymanager.h"
#include "parasolid/operations/operation.h"

using namespace pk;

bool ConstractiveOperations::makeBlend(QVector<PK_EDGE_t> edges, double radius)
{
    PK_EDGE_set_blend_constant_o_t options;
    PK_EDGE_set_blend_constant_o_m(options);
    int nBlended = 0;
    PK_EDGE_t* blended;
    LOG(TRACE) << "Blending edges " << edges << " with radius " << radius;
    PK_ERROR_t error = PK_EDGE_set_blend_constant(edges.length(), edges.data(), radius, &options, &nBlended, &blended);
    if (error != PK_ERROR_no_errors) {
        return false;
    }

    QVector<PK_BODY_t> affectedBodys = getEdgesBodys(edges);
    fixBlends(affectedBodys);

    Operation op("MakeBlend", { { "target", QVariant::fromValue(edges) }, { "radius", QVariant::fromValue(radius) } });
    EntityManager::Instance().getBuildTree().addOperation(op);
    return true;
}

bool ConstractiveOperations::makeChamfer(QVector<PK_EDGE_t> edges, double size)
{
    PK_EDGE_set_blend_chamfer_o_t options;
    PK_EDGE_set_blend_chamfer_o_m(options);
    int nChamfered = 0;
    PK_EDGE_t* chamfered;
    LOG(TRACE) << "Chamfering edges " << edges << " with size " << size;
    PK_ERROR_t error = PK_EDGE_set_blend_chamfer(edges.length(), edges.data(), size, size, NULL, &options, &nChamfered, &chamfered);
    if (error != PK_ERROR_no_errors) {
        return false;
    }

    QVector<PK_BODY_t> affectedBodys = getEdgesBodys(edges);
    fixBlends(affectedBodys);

    Operation op("MakeChamfer", { { "target", QVariant::fromValue(edges) }, { "size", QVariant::fromValue(size) } });
    EntityManager::Instance().getBuildTree().addOperation(op);
    return true;
}

void ConstractiveOperations::fixBlends(QVector<PK_BODY_t> body)
{
    PK_BODY_fix_blends_o_t fix_opts;
    int nBlends = 0;
    PK_FACE_t* pBlends = NULL;
    PK_FACE_array_t* pFaces = NULL;
    int* pEdges = NULL;
    int nParts;
    PK_blend_fault_t fault = PK_blend_fault_no_fault_c;
    PK_EDGE_t fault_edge = PK_ENTITY_null;
    PK_TOPOL_t fault_topol = PK_ENTITY_null;
    PK_PART_t* parts;
    PK_SESSION_ask_parts(&nParts, &parts);
    PK_BODY_fix_blends_o_m(fix_opts);

    for (PK_BODY_t b : body) {
        PK_BODY_fix_blends(b, &fix_opts, &nBlends, &pBlends, &pFaces, &pEdges, &fault, &fault_edge, &fault_topol);
    }
    if (nParts > 0) {
        PK_MEMORY_free(parts);
    }

    if (nBlends) {
        PK_MEMORY_free(pBlends);
        PK_MEMORY_free(pFaces);
        PK_MEMORY_free(pEdges);
    }
}

QVector<PK_BODY_t> ConstractiveOperations::getEdgesBodys(QVector<PK_EDGE_t> edges)
{
    QVector<PK_BODY_t> affectedBodys;
    for (PK_EDGE_t edge : edges) {
        PK_BODY_t b;
        PK_EDGE_ask_body(edge, &b);
        affectedBodys.push_back(b);
    }

    return affectedBodys;
}
