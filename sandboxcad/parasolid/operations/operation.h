#ifndef OPERATION_H
#define OPERATION_H

#include <QString>
#include <QVariant>

namespace pk {

class Operation
{
public:
    Operation() {}
    Operation(QString name, QMap<QString, QVariant> params);
    QMap<QString, QVariant> parameters() const;
    QString operationName() const;

private:
    QString m_operationName;
    QMap<QString, QVariant> m_parameters;
};
}

#endif // OPERATION_H
