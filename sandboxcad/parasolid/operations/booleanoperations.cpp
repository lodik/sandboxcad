#include "booleanoperations.h"

#include "easylogging++.h"

#include "parasolid/entitymanager.h"
#include "parasolid/operations/operation.h"

using namespace pk;

bool BooleanOperations::preformOperation(BooleanOperations::Operation operationType, PK_BODY_t target, QVector<PK_BODY_t> tools)
{
    bool result = true;
    switch (operationType) {
    case UNITE:
        result = unite(target, tools);
        break;
    case SUBTRACT:
        result = subtract(target, tools);
        break;
    case INTERSECT:
        result = intersect(target, tools);
        break;
    default:
        result = false;
        break;
    }
    return result;
}

bool BooleanOperations::unite(PK_BODY_t target, QVector<PK_BODY_t> tools)
{
    PK_BODY_boolean_o_t bool_opts;
    PK_TOPOL_track_r_t tracking;
    PK_boolean_r_t results;
    PK_BODY_boolean_o_m(bool_opts);
    bool_opts.function = PK_boolean_unite_c;

    LOG(INFO) << "Uniting body " << target << " with " << tools;
    PK_ERROR_t error = PK_BODY_boolean_2(target, tools.length(), tools.data(), &bool_opts, &tracking, &results);
    if (error != PK_ERROR_no_errors) {
        return false;
    }
    pk::Operation op("Unite", { {"target", QVariant::fromValue(target)}, {"tools", QVariant::fromValue(tools)} });
    EntityManager::Instance().getBuildTree().addOperation(op);
    return true;
}


bool BooleanOperations::subtract(PK_BODY_t target, QVector<PK_BODY_t> tools)
{
    PK_BODY_boolean_o_t bool_opts;
    PK_TOPOL_track_r_t tracking;
    PK_boolean_r_t results;
    PK_BODY_boolean_o_m(bool_opts);
    bool_opts.function = PK_boolean_subtract_c;

    LOG(INFO) << "Substracting body " << target << " with " << tools;
    PK_ERROR_t error = PK_BODY_boolean_2(target, tools.length(), tools.data(), &bool_opts, &tracking, &results);
    if (error != PK_ERROR_no_errors) {
        return false;
    }
    pk::Operation op("Subtract", { {"target", QVariant::fromValue(target)}, {"tools", QVariant::fromValue(tools)} });
    EntityManager::Instance().getBuildTree().addOperation(op);
    return true;
}

bool BooleanOperations::intersect(PK_BODY_t target, QVector<PK_BODY_t> tools)
{
    PK_BODY_boolean_o_t bool_opts;
    PK_TOPOL_track_r_t tracking;
    PK_boolean_r_t results;
    PK_BODY_boolean_o_m(bool_opts);
    bool_opts.function = PK_boolean_intersect_c;

    LOG(INFO) << "Intersecting body " << target << " with " << tools;
    PK_ERROR_t error = PK_BODY_boolean_2(target, tools.length(), tools.data(), &bool_opts, &tracking, &results);
    if (error != PK_ERROR_no_errors) {
        return false;
    }
    pk::Operation op("Intersect", { {"target", QVariant::fromValue(target)}, {"tools", QVariant::fromValue(tools)} });
    EntityManager::Instance().getBuildTree().addOperation(op);
    return true;
}
