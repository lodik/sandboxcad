#include "blockoperations.h"
#include "easylogging++.h"
#include "operation.h"
#include "parasolid/entitymanager.h"

Q_DECLARE_METATYPE(PK_AXIS2_sf_s)
Q_DECLARE_METATYPE(PK_BODY_t)


void BlockOperations::createSolidBlock(double x, double y, double z, const PK_AXIS2_sf_s *axis, PK_BODY_t *body)
{

    PK_BODY_create_solid_block(x, y ,z, axis, body);
    LOG(INFO) << "Creating solid block with size [" << x << ", " << y << ", " << z << "] with csys " << "{axis: ["
                    << axis->axis.coord[0] << ", " << axis->axis.coord[1] << ", " << axis->axis.coord[2] << "]; ref_dir: ["
                    << axis->ref_direction.coord[0] << ", " << axis->ref_direction.coord[1] << ", " << axis->ref_direction.coord[2] << "]; loc: ["
                    << axis->location.coord[0] << ", " << axis->location.coord[1] << ", " << axis->location.coord[2] <<"]}"
                    << ". Result body id " << *body;
    pk::Operation op("Cube", {
                         { "size.x", QVariant::fromValue(x) },
                         { "size.y", QVariant::fromValue(y) },
                         { "size.z", QVariant::fromValue(z) },
                         { "pos.axis", QVariant::fromValue(*axis) },
                         { "result", QVariant::fromValue(*body) }
                     });
    pk::EntityManager::Instance().getBuildTree().addOperation(op);
}

void BlockOperations::createSolidSphere(double radius, const PK_AXIS2_sf_s *axis, PK_BODY_t *body)
{
    PK_BODY_create_solid_sphere(radius, axis, body);
    LOG(INFO) << "Creating solid sphere with [ radius = " << radius << " ] with csys " << "{axis: ["
                    << axis->axis.coord[0] << ", " << axis->axis.coord[1] << ", " << axis->axis.coord[2] << "]; ref_dir: ["
                    << axis->ref_direction.coord[0] << ", " << axis->ref_direction.coord[1] << ", " << axis->ref_direction.coord[2] << "]; loc: ["
                    << axis->location.coord[0] << ", " << axis->location.coord[1] << ", " << axis->location.coord[2] <<"]}"
                    << ". Result body id " << *body;
    pk::Operation op("Sphere", {
                         { "size.rad", QVariant::fromValue(radius) },
                         { "pos.axis", QVariant::fromValue(*axis) },
                         { "result", QVariant::fromValue(*body) }
                     });
    pk::EntityManager::Instance().getBuildTree().addOperation(op);
}

void BlockOperations::createSolidCone(double lradius, double height, double angle, const PK_AXIS2_sf_s *axis, PK_BODY_t *body)
{
    PK_BODY_create_solid_cone(lradius, height, angle, axis, body);
    LOG(INFO) << "Creating solid cone with [ radius = " << lradius << "; height = " << height << "; angle = " << angle << " ] with csys " << "{axis: ["
                    << axis->axis.coord[0] << ", " << axis->axis.coord[1] << ", " << axis->axis.coord[2] << "]; ref_dir: ["
                    << axis->ref_direction.coord[0] << ", " << axis->ref_direction.coord[1] << ", " << axis->ref_direction.coord[2] << "]; loc: ["
                    << axis->location.coord[0] << ", " << axis->location.coord[1] << ", " << axis->location.coord[2] <<"]}"
                    << ". Result body id " << *body;
    pk::Operation op("Cone", {
                         { "size.lrad", QVariant::fromValue(lradius) },
                         { "size.h", QVariant::fromValue(height) },
                         { "size.angle", QVariant::fromValue(angle) },
                         { "pos.axis", QVariant::fromValue(*axis) },
                         { "result", QVariant::fromValue(*body) }
                     });
    pk::EntityManager::Instance().getBuildTree().addOperation(op);
}

void BlockOperations::createSolidCylinder(double radius, double height, const PK_AXIS2_sf_s *axis, PK_BODY_t *body)
{
    PK_BODY_create_solid_cyl(radius, height, axis, body);
    LOG(INFO) << "Creating solid cylinger with [ radius = " << radius << "; height = " << height << " ] with csys " << "{axis: ["
                    << axis->axis.coord[0] << ", " << axis->axis.coord[1] << ", " << axis->axis.coord[2] << "]; ref_dir: ["
                    << axis->ref_direction.coord[0] << ", " << axis->ref_direction.coord[1] << ", " << axis->ref_direction.coord[2] << "]; loc: ["
                    << axis->location.coord[0] << ", " << axis->location.coord[1] << ", " << axis->location.coord[2] <<"]}"
                    << ". Result body id " << *body;
    pk::Operation op("Cylinder", {
                         { "size.rad", QVariant::fromValue(radius) },
                         { "size.h", QVariant::fromValue(height) },
                         { "pos.axis", QVariant::fromValue(*axis) },
                         { "result", QVariant::fromValue(*body) }
                     });
    pk::EntityManager::Instance().getBuildTree().addOperation(op);
}

void BlockOperations::createSolidPrism(double radius, double height, int sides, const PK_AXIS2_sf_t *axis,  PK_BODY_t *body)
{
    PK_BODY_create_solid_prism(radius, height, sides, axis, body);
    LOG(INFO) << "Creating solid prism with [ radius = " << radius << "; height = " << height << "; sides = " << sides << " ] with csys " << "{axis: ["
                    << axis->axis.coord[0] << ", " << axis->axis.coord[1] << ", " << axis->axis.coord[2] << "]; ref_dir: ["
                    << axis->ref_direction.coord[0] << ", " << axis->ref_direction.coord[1] << ", " << axis->ref_direction.coord[2] << "]; loc: ["
                    << axis->location.coord[0] << ", " << axis->location.coord[1] << ", " << axis->location.coord[2] <<"]}"
                    << ". Result body id " << *body;
    pk::Operation op("Prism", {
                         { "size.rad", QVariant::fromValue(radius) },
                         { "size.h", QVariant::fromValue(height) },
                         { "size.sides", QVariant::fromValue(sides) },
                         { "pos.axis", QVariant::fromValue(*axis) },
                         { "result", QVariant::fromValue(*body) }
                     });
    pk::EntityManager::Instance().getBuildTree().addOperation(op);
}

void BlockOperations::createSolidTorus(double radiusMajor, double radiusMinor, const PK_AXIS2_sf_t *axis, PK_BODY_t *body)
{
    PK_BODY_create_solid_torus(radiusMajor, radiusMinor, axis, body);
    LOG(INFO) << "Creating solid torus with [ major radius = " << radiusMajor << "; minor radius = " << radiusMinor << " ] with csys " << "{axis: ["
                    << axis->axis.coord[0] << ", " << axis->axis.coord[1] << ", " << axis->axis.coord[2] << "]; ref_dir: ["
                    << axis->ref_direction.coord[0] << ", " << axis->ref_direction.coord[1] << ", " << axis->ref_direction.coord[2] << "]; loc: ["
                    << axis->location.coord[0] << ", " << axis->location.coord[1] << ", " << axis->location.coord[2] <<"]}"
                    << ". Result body id " << *body;
    pk::Operation op("Torus", {
                         { "size.rad.mj", QVariant::fromValue(radiusMajor) },
                         { "size.rad.mn", QVariant::fromValue(radiusMinor) },
                         { "pos.axis", QVariant::fromValue(*axis) },
                         { "result", QVariant::fromValue(*body) }
                     });
    pk::EntityManager::Instance().getBuildTree().addOperation(op);
}
