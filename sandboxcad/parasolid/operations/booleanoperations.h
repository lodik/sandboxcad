#ifndef BOOLEANOPERATIONS_H
#define BOOLEANOPERATIONS_H

#include "parasolid_kernel.h"
#include <QVector>

class BooleanOperations
{
    enum Operation {
        UNITE,
        SUBTRACT,
        INTERSECT
    };

public:
    static bool unite(PK_BODY_t target, QVector<PK_BODY_t> tools);
    static bool subtract(PK_BODY_t target, QVector<PK_BODY_t> tools);
    static bool intersect(PK_BODY_t target, QVector<PK_BODY_t> tools);

    static bool preformOperation(Operation operationType, PK_BODY_t target, QVector<PK_BODY_t> tools);
};

#endif // BOOLEANOPERATIONS_H
