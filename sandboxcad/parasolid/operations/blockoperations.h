#ifndef BLOCKOPERATIONS_H
#define BLOCKOPERATIONS_H

#include <QtCore>
#include "parasolid_kernel.h"


class BlockOperations
{
public:
    static void createSolidBlock(double x, double y, double z, const PK_AXIS2_sf_s *axis, PK_BODY_t *body);
    static void createSolidSphere(double radius, const PK_AXIS2_sf_s *axis, PK_BODY_t *body);
    static void createSolidCone(double lradius, double height, double angle, const PK_AXIS2_sf_s *axis, PK_BODY_t *body);
    static void createSolidCylinder(double radius, double height, const PK_AXIS2_sf_s *axis, PK_BODY_t *body);
    static void createSolidPrism(double radius, double height, int sides, const PK_AXIS2_sf_t *axis,  PK_BODY_t *body);
    static void createSolidTorus(double radiusMajor, double radiusMinor, const PK_AXIS2_sf_t *axis,  PK_BODY_t *body);
};

#endif // BLOCKOPERATIONS_H
