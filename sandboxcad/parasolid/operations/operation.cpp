#include "operation.h"

using namespace pk;


Operation::Operation(QString name, QMap<QString, QVariant> params) :
    m_operationName(name), m_parameters(params)
{

}

QMap<QString, QVariant> Operation::parameters() const
{
    return m_parameters;
}

QString Operation::operationName() const
{
    return m_operationName;
}
