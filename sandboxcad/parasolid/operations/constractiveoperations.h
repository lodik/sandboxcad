#ifndef CONSTRACTIVEOPERATIONS_H
#define CONSTRACTIVEOPERATIONS_H

#include "parasolid_kernel.h"
#include <QVector>

class ConstractiveOperations
{
public:
    static bool makeBlend(QVector<PK_EDGE_t> edges, double radius);
    static bool makeChamfer(QVector<PK_EDGE_t> edges, double size);

private:
    static void fixBlends(QVector<PK_BODY_t> body);
    static QVector<PK_BODY_t> getEdgesBodys(QVector<PK_EDGE_t> edges);
};

#endif // CONSTRACTIVEOPERATIONS_H
