#include "psaver.h"

#include "easylogging++.h"
#include "parasolid_kernel.h"

void PSaver::save(QString path)
{
    PK_PART_transmit_o_t transmit_opts;
    int nParts;
    PK_PART_t* parts;
    PK_SESSION_ask_parts(&nParts, &parts);

    PK_PART_transmit_o_m(transmit_opts);
    transmit_opts.transmit_format = PK_transmit_format_text_c;

    LOG(INFO) << "Saving few parts to " << path;
    PK_PART_transmit(nParts, parts, path.toStdString().c_str(), &transmit_opts);
}

void PSaver::load(QString path)
{
    const char* key = path.toStdString().c_str();
    PK_PART_receive_o_t receive_opts;
    PK_PART_receive_o_m(receive_opts);
    receive_opts.transmit_format = PK_transmit_format_text_c;
    int n_parts = 0;
    PK_BODY_t* body = NULL;
    LOG(INFO) << "Loading few parts from " << path;
    PK_PART_receive(key, &receive_opts, &n_parts, &body);
    if (n_parts > 0) {
        PK_MEMORY_free(body);
    }
}
