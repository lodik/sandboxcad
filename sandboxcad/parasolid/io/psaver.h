#ifndef PSAVER_H
#define PSAVER_H

#include <QtCore>

class PSaver
{
public:
    static void save(QString path);
    static void load(QString path);
};

#endif // PSAVER_H
