#include "ppicker.h"

#include "parasolid_kernel.h"
#include "psession.h"
#include "easylogging++.h"

QVector<int> PPicker::pickElements(Qt3D::QRay3D ray, double radius)
{
    PK_AXIS1_sf_t _ray;
    PK_VECTOR1_t location = { ray.origin().x(), ray.origin().y(), ray.origin().z() };
    PK_VECTOR1_t axis = { ray.direction().x(), ray.direction().y(), ray.direction().z() };
    _ray.location = location;
    _ray.axis = axis;

    PK_BODY_pick_topols_o_t pickOptions;
    PK_BODY_pick_topols_o_m(pickOptions);

    int nBodies;
    PK_BODY_t *bodies;
    PK_PARTITION_ask_bodies(partmgr.currentPartition().partition, &nBodies, &bodies);

    PK_BODY_pick_topols_r_t picked;
    PK_BODY_pick_topols(nBodies, bodies, nullptr, &_ray, &pickOptions, &picked);

    LOG(TRACE) << "Picked " << picked.n_faces << " faces; " << picked.n_edges << " edges; " << picked.n_vertices << " vertexes";

    QVector<int> result;

    for (int i = 0; i < picked.n_edges; i++) {
        result.append(picked.edges[i].entity);
    }
    for (int i = 0; i < picked.n_faces; i++) {
        result.append(picked.faces[i].entity);
    }
    for (int i = 0; i < picked.n_vertices; i++) {
        result.append(picked.vertices[i].entity);
    }

    return qMove(result);
}
