#ifndef FRUSTRUMGO_H
#define FRUSTRUMGO_H

#include "parasolid_kernel.h"
#include <QVector>
#include <QPair>
#include <QMap>

#include "bodyinfo.h"

#include "app/ui/settings.h"

class FrustrumGO
{
public:
    static QPair<QVector<double>, QVector<double> > getBodyInfo(PK_BODY_t body);
    static QVector<QVector<double>> getWireBodyInfo(PK_BODY_t body);
    static QMap<PK_BODY_t, BodyInfo> getBodiesInfo(const QVector<PK_BODY_t> &bodies);

private:
    static void setDefaultOutputOptions(PK_TOPOL_facet_2_o_t* options);
    static QVector<double> getCurveData(PK_CURVE_t curve, PK_VECTOR_t *from, PK_VECTOR_t *to);
};

#endif // FRUSTRUMGO_H
