#ifndef BODYINFO_H
#define BODYINFO_H

#include <QVector>
#include <QPair>

class BodyInfo
{
public:
    BodyInfo();

    QVector<double> vertexes() const;
    void setVertexes(QVector<double> &&vertexes);

    QVector<double> normals() const;
    void setNormals(QVector<double> &&normals);

    QVector<double> colour() const;
    void setColour(QVector<double> &&colour);

    QVector<QVector<double> > wire() const;
    void setWire(QVector<QVector<double> > &&wire);

    QVector<double> edges() const;
    void setEdges(const QVector<double> &&edges);

private:
    QVector<double> m_vertexes;
    QVector<double> m_normals;
    QVector<double> m_colour;
    QVector<double> m_edges;
    QVector<QVector<double>> m_wire;
};

typedef QPair<int, BodyInfo> BodyInfoEntity;

#endif // BODYINFO_H
