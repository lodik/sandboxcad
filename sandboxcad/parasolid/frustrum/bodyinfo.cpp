#include "bodyinfo.h"

BodyInfo::BodyInfo()
{

}

QVector<double> BodyInfo::vertexes() const
{
    return m_vertexes;
}

void BodyInfo::setVertexes(QVector<double> &&vertexes)
{
    m_vertexes = qMove(vertexes);
}
QVector<double> BodyInfo::normals() const
{
    return m_normals;
}

void BodyInfo::setNormals(QVector<double> &&normals)
{
    m_normals = qMove(normals);
}
QVector<double> BodyInfo::colour() const
{
    return m_colour;
}

void BodyInfo::setColour(QVector<double> &&colour)
{
    m_colour = qMove(colour);
}
QVector<QVector<double> > BodyInfo::wire() const
{
    return m_wire;
}

void BodyInfo::setWire(QVector<QVector<double> > &&wire)
{
    m_wire = qMove(wire);
}
QVector<double> BodyInfo::edges() const
{
    return m_edges;
}

void BodyInfo::setEdges(const QVector<double> &&edges)
{
    m_edges = qMove(edges);
}






