#include "godatacollector.h"

QVector<double> GODataCollector::vertexes;
QVector<double> GODataCollector::normals;
QVector<QVector<double>> GODataCollector::edges;

GODataCollector::GODataCollector()
{

}

void GODataCollector::addVertexes(double value) {
    vertexes.push_back(value);
}

QVector<double> GODataCollector::getVertexes() {
    return vertexes;
}

QVector<double> GODataCollector::getNormals()
{
    return normals;
}

QVector<QVector<double>> GODataCollector::getEdges()
{
    return edges;
}

void GODataCollector::addEdges(QVector<double> &&value)
{
    edges.push_back(qMove(value));
}

void GODataCollector::addNormals(double value)
{
    normals.push_back(value);
}

void GODataCollector::clear()
{
    vertexes.clear();
    normals.clear();
    edges.clear();
}
