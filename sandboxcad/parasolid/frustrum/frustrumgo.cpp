#include "frustrumgo.h"

#include "easylogging++.h"
#include "godatacollector.h"
#include <QtConcurrent>

static const int SAMPLES = 32;

QPair<QVector<double>, QVector<double> > FrustrumGO::getBodyInfo(PK_BODY_t body)
{
    QVector<double> vertexes;
    QVector<double> normals;

    PK_TOPOL_facet_2_o_t options;
    PK_TOPOL_facet_2_o_m(options);
    setDefaultOutputOptions(&options);

    PK_TOPOL_fctab_facet_fin_s* facetFin;
    PK_TOPOL_fctab_fin_data_s* finData;
    PK_TOPOL_fctab_data_point_s* dataPointIdX;
    PK_TOPOL_fctab_data_normal_s* dataNormalIdX;
    PK_TOPOL_fctab_point_vec_s* pointVec;
    PK_TOPOL_fctab_normal_vec_s* normalVec;

    int topolCount;
    PK_TOPOL_t* topols;
    PK_BODY_ask_faces(body, &topolCount, &topols);
    PK_TOPOL_facet_2_r_t output;
    PK_TOPOL_facet_2(topolCount, topols, NULL, &options, &output);

    for (int i = 0; i < output.number_of_tables; i++) {
        switch (output.tables[i].fctab) {
        case PK_TOPOL_fctab_facet_fin_c:
            facetFin = output.tables[i].table.facet_fin;
            break;
        case PK_TOPOL_fctab_fin_data_c:
            finData = output.tables[i].table.fin_data;
            break;
        case PK_TOPOL_fctab_data_point_c:
            dataPointIdX = output.tables[i].table.data_point_idx;
            break;
        case PK_TOPOL_fctab_data_normal_c:
            dataNormalIdX = output.tables[i].table.data_normal_idx;
            break;
        case PK_TOPOL_fctab_point_vec_c:
            pointVec = output.tables[i].table.point_vec;
            break;
        case PK_TOPOL_fctab_normal_vec_c:
            normalVec = output.tables[i].table.normal_vec;
            break;
        default:
            break;
        }
    }

    QMultiMap<int, int> wFacetFins;
    for (int i = 0; i < facetFin->length; i++) {
        wFacetFins.insert(facetFin->data[i].facet, facetFin->data[i].fin);
    }

    for (int facet : wFacetFins.keys().toSet()) {
        for (int fin : wFacetFins.values(facet)) {
            int finIndex = finData->data[fin];
            int pointIndex = dataPointIdX->point[finIndex];
            int normalIndex = dataNormalIdX->normal[finIndex];
            double* vecCoords = pointVec->vec[pointIndex].coord;
            double* vecNormals = normalVec->vec[normalIndex].coord;
            vertexes.append(vecCoords[0]); vertexes.append(vecCoords[1]); vertexes.append(vecCoords[2]);
            normals.append(vecNormals[0]); normals.append(vecNormals[1]); normals.append(vecNormals[2]);
        }
    }
    LOG(TRACE) << "Body{" << body << "} vertex count = " << vertexes.size() / 3 << "; normals count = " << normals.size() / 3;
    auto result = QPair<QVector<double>, QVector<double> >(vertexes, normals);
    return qMove(result);
}

QVector<QVector<double> > FrustrumGO::getWireBodyInfo(PK_BODY_t body)
{
    QVector<QVector<double> > result;
    int nEdges;
    PK_EDGE_t *edges;
    PK_BODY_ask_edges(body, &nEdges, &edges);

    for (int i = 0; i < nEdges; i++) {
        PK_CURVE_t curve;
        PK_EDGE_ask_curve(edges[i], &curve);
        PK_PARAM_sf_s curve_param;
        PK_CURVE_ask_param(curve, &curve_param);
        PK_VERTEX_t vertexes[2];
        PK_EDGE_ask_vertices(edges[i], vertexes);
        if (curve_param.periodic == PK_PARAM_periodic_no_c || ((vertexes[0] != PK_ENTITY_null && vertexes[1] != PK_ENTITY_null))) {
            //standart complex line
            PK_VECTOR_t vertexesCoords[2];
            for (int j = 0; j < 2; j++) {
                PK_POINT_t point;
                PK_VERTEX_ask_point(vertexes[j], &point);
                PK_POINT_sf_s pointData;
                PK_POINT_ask(point, &pointData);
                vertexesCoords[j] = pointData.position;
            }
            result.append(qMove(getCurveData(curve, &vertexesCoords[0], &vertexesCoords[1])));
        } else {
            //circle or ellipce, etc
            result.append(qMove(getCurveData(curve, NULL, NULL)));
        }
    }
    return qMove(result);
}



QMap<PK_BODY_t, BodyInfo> FrustrumGO::getBodiesInfo(const QVector<PK_BODY_t> &bodies)
{
    QMap<PK_BODY_t, BodyInfo> result;
    /*
    QFuture<BodyInfoEntity> bodiesInfo = QtConcurrent::mapped(bodies, *[] (PK_BODY_t body) {
        LOG(TRACE) << "Getting body info for body " << body;
        BodyInfo bodyData;
        auto vertAndNormals = FrustrumGO::getBodyInfo(body);
        auto wire = FrustrumGO::getWireBodyInfo(body);

        bodyData.setVertexes(qMove(vertAndNormals.first));
        bodyData.setNormals(qMove(vertAndNormals.second));
        bodyData.setWire(qMove(wire));
        return qMakePair(body, qMove(bodyData));
    });
    bodiesInfo.waitForFinished();

    for (BodyInfoEntity entity : bodiesInfo) {
        result.insert(entity.first, qMove(entity.second));
    }
    */

    for (PK_BODY_t body : bodies) {
        PK_TOPOL_render_facet_o_t renderOptions;
        PK_TOPOL_render_facet_o_m(renderOptions);
        renderOptions.go_option.go_normals = PK_facet_go_normals_yes_c;
        renderOptions.control.quality = PK_facet_quality_improved_c;
        renderOptions.control.vertices_on_planar = PK_LOGICAL_true;
        renderOptions.control.match = PK_facet_match_topol_c;
        renderOptions.control.max_facet_sides = 3;
        renderOptions.control.is_surface_plane_tol = PK_LOGICAL_true;
        renderOptions.control.surface_plane_tol = appSettings.tolerance;
        PK_TOPOL_render_facet(1, &body, nullptr, PK_ENTITY_null, &renderOptions);

        PK_TOPOL_render_line_o_t renderOptionsWire;
        PK_TOPOL_render_line_o_m(renderOptionsWire);
        renderOptionsWire.planar = PK_render_planar_attrib_c;
        renderOptionsWire.radial = PK_render_radial_attrib_c;
        renderOptionsWire.param  = PK_render_param_attrib_c;
        PK_TOPOL_render_line(1, &body, nullptr, PK_ENTITY_null, &renderOptionsWire);

        BodyInfo bodyData;
        bodyData.setVertexes(qMove(GODataCollector::getVertexes()));
        bodyData.setNormals(qMove(GODataCollector::getNormals()));
        bodyData.setWire(qMove(GODataCollector::getEdges()));
        result.insert(body, qMove(bodyData));
        GODataCollector::clear();
    }

    return qMove(result);
}

void FrustrumGO::setDefaultOutputOptions(PK_TOPOL_facet_2_o_t* options)
{
    options->choice.facet_fin = PK_LOGICAL_true;
    options->choice.data_normal_idx = PK_LOGICAL_true;
    options->choice.normal_vec = PK_LOGICAL_true;
    options->choice.data_normal_idx = PK_LOGICAL_true;
    options->choice.fin_data = PK_LOGICAL_true;
    options->choice.point_vec = PK_LOGICAL_true;
    options->choice.data_point_idx = PK_LOGICAL_true;
    options->choice.fin_edge = PK_LOGICAL_true;

    options->control.quality = PK_facet_quality_improved_c;
    options->control.vertices_on_planar = PK_LOGICAL_true;
    options->control.match = PK_facet_match_topol_c;
    options->control.max_facet_sides = 3;
    options->control.is_surface_plane_tol = PK_LOGICAL_true;
    options->control.surface_plane_tol = 0.1;
    //options->control.is_max_facet_width = PK_LOGICAL_true;
    //options->control.max_facet_width = 0.5;
}

QVector<double> FrustrumGO::getCurveData(PK_CURVE_t curve, PK_VECTOR_t *from, PK_VECTOR_t *to)
{
    QVector<double> result;
    PK_INTERVAL_t interval;
    if (from == NULL && to == NULL) {
        PK_CURVE_ask_interval(curve, &interval);
    } else {
        PK_CURVE_find_vector_interval(curve, *from, *to, &interval);
    }
    double totalL = interval.value[1] - interval.value[0];
    double delta = totalL / 32;
    for (double i = 0; i < totalL + 0.01; i += delta) {
        PK_VECTOR_t coords;
        PK_CURVE_eval(curve, interval.value[0] + i, 0, &coords);
        result.append(coords.coord[0]); result.append(coords.coord[1]); result.append(coords.coord[2]);
    }
    return qMove(result);
}
