#ifndef GODATACOLLECTOR_H
#define GODATACOLLECTOR_H

#include <QVector>

class GODataCollector
{
public:
    GODataCollector();
    static QVector<double> getVertexes();
    static void addVertexes(double value);
    static QVector<double> getNormals();
    static void addNormals(double value);
    static QVector<QVector<double>> getEdges();
    static void addEdges(QVector<double> &&value);

    static void clear();

    static QVector<double> vertexes;
    static QVector<double> normals;
    static QVector<QVector<double>> edges;

};

#endif // GODATACOLLECTOR_H
