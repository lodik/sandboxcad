#ifndef PFRUSTRUM
#define PFRUSTRUM

#include "parasolid_kernel.h"

namespace pk {
namespace frustrum {

    extern void startFileFrustrum(int *);
    extern void abortFrustrum(int *);
    extern void stopFileFrustrum(int *);
    extern void openReadFrustrumFile(const int *, const int *, const char *, const int *, const int *, int *, int *);
    extern void openWriteFrustrumFile(const int *, const int *, const char *,const int *, const char *, const int *, int *, int *);
    extern void readFromFrustrumFile(const int *, const int *, const int *, char *, int *, int *);
    extern void writeToFrustrumFile(const int *, const int *, const int *, const char *, int *);
    extern void closeFrustrumFile(const int *, const int *, const int *, int *);

    extern void openSegment(const int *segtyp, const int *ntags, const int *tags, const int *ngeoms, const double *geoms, const int *nlntp, const int *lntp, int *ifail);
    extern void closeSegment(const int *segtyp, const int *ntags, const int *tags, const int *ngeoms, const double *geoms, const int *nlntp, const int *lntp, int *ifail);
    extern void outputSegment(const int *segtyp, const int *ntags, const int *tags, const int *ngeoms, const double *geoms, const int *nlntp, const int *lntp, int *ifail);

    extern PK_ERROR_code_t FRU_delta_open_for_write(PK_PMARK_t, PK_DELTA_t *);
    extern PK_ERROR_code_t FRU_delta_open_for_read(PK_DELTA_t);
    extern PK_ERROR_code_t FRU_delta_write(PK_DELTA_t, unsigned, const char *);
    extern PK_ERROR_code_t FRU_delta_read(PK_DELTA_t, unsigned, char *);
    extern PK_ERROR_code_t FRU_delta_delete(PK_DELTA_t);
    extern PK_ERROR_code_t FRU_delta_close(PK_DELTA_t);
    extern int FRU__delta_init(int action);

}
}

#endif // PFRUSTRUM

