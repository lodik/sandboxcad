#include "psession.h"

#include "parasolid_kernel.h"
#include "frustrum_ifails.h"

#include "easylogging++.h"
#include "frustrum/pfrustrum.h"


using namespace pk;
using namespace pk::frustrum;


PSession::PSession()
{

}


PSession &PSession::Instance()
{
    static PSession m_instance;
    return m_instance;
}

bool PSession::createSession()
{
    bool ok = true;

    PK_SESSION_frustrum_t fru;
    PK_SESSION_frustrum_o_m(fru);

    // Register frustrum functions
    fru.fstart = startFrustrum;
    fru.fabort = abortFrustrum;
    fru.fstop = stopFrustrum;
    fru.fmallo = getMemory;
    fru.fmfree = returnMemory;
    fru.ffoprd = openReadFrustrumFile;
    fru.ffopwr = openWriteFrustrumFile;
    fru.ffclos = closeFrustrumFile;
    fru.ffread = readFromFrustrumFile;
    fru.ffwrit = writeToFrustrumFile;

    fru.gosgmt = outputSegment;
    fru.goopsg = openSegment;
    fru.goclsg = closeSegment;

    PK_SESSION_register_frustrum(&fru);
    LOG(DEBUG) << "Frustrum function registred";

    PK_DELTA_frustrum_t delta_fru;

    // Register Delta Frustrum
    delta_fru.open_for_write_fn = FRU_delta_open_for_write;
    delta_fru.open_for_read_fn = FRU_delta_open_for_read;
    delta_fru.close_fn = FRU_delta_close;
    delta_fru.write_fn = FRU_delta_write;
    delta_fru.read_fn = FRU_delta_read;
    delta_fru.delete_fn = FRU_delta_delete;

    if (PK_DELTA_register_callbacks(delta_fru) != PK_ERROR_no_errors) {
        return false;
    }
    LOG(DEBUG) << "Delta Frustrum function registred";

    PK_ERROR_frustrum_t errorFru;
    // Register Error Handler
    errorFru.handler_fn = PKerrorHandler;
    if (PK_ERROR_register_callbacks(errorFru) != PK_ERROR_no_errors) {
        return false;
    }
    LOG(DEBUG) << "Error Handler function registred";


    // Starts the modeller
    PK_SESSION_start_o_t options;
    PK_SESSION_start_o_m(options);

    PK_SESSION_start(&options);

    // Check to see if it all started up OK
    PK_LOGICAL_t was_error = PK_LOGICAL_true;
    PK_ERROR_sf_t error_sf;
    PK_ERROR_ask_last(&was_error, &error_sf);
    if (was_error)
        return false;
    LOG(INFO) << "Parasolid session created";

//    int partitionsCount;
//    PK_PARTITION_t *partitions;
//    PK_SESSION_ask_partitions(&partitionsCount, &partitions);
//    m_partitions.insert("default", partitions[0]);


    return ok;
}

bool PSession::stopSession()
{
    PK_SESSION_stop();
    return true;
}


void PSession::startFrustrum(int *ifail)
{
    *ifail = FR_no_errors;
    FRU__delta_init(1);
    startFileFrustrum(ifail);
}


void PSession::stopFrustrum(int *ifail)
{

    *ifail = FR_no_errors;
    FRU__delta_init(2);
    stopFileFrustrum(ifail);
}


void PSession::getMemory(int *nBytes, char **memory, int *ifail)
{

    *memory = new char[*nBytes];
    *ifail = (*memory) ? FR_no_errors : FR_memory_full;
}


void PSession::returnMemory(int *nBytes, char **memory, int *ifail)
{
    delete[] * memory;
    *ifail = FR_no_errors;
}


PK_ERROR_code_t PSession::PKerrorHandler(PK_ERROR_sf_t *error)
{
    LOG(WARNING) << "Error while preforming " << error->function << ". Error code: "
              << error->code_token << ".";
    return error->code;
}
