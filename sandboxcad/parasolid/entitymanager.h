#ifndef ENTITYMANAGER_H
#define ENTITYMANAGER_H

#include <QVector3D>
#include <QVector>
#include <QMapIterator>

#include "parasolid_kernel.h"

#include "parasolid/buildtree.h"

namespace pk {

class EntityManager {
  public:
    static EntityManager &Instance();

    void update();
    QVector<PK_BODY_t> getBodies();
    QVector<PK_EDGE_t> getEdges();
    QVector<PK_GEOM_t> getGeom();
    BuildTree &getBuildTree();

    void createTempEntity();
private:
    EntityManager();

private:
    QList<PK_BODY_t> m_bodies;
    BuildTree m_buildTree;
};
}


#endif // ENTITYMANAGER_H
