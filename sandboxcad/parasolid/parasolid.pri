HEADERS += \
    $$PWD/entitymanager.h \
    $$PWD/psession.h \
    $$PWD/frustrum/pfrustrum.h \
    $$PWD/frustrum/frustrumgo.h \
    $$PWD/operations/booleanoperations.h \
    $$PWD/sketchs/sketcher.h \
    $$PWD/operations/constractiveoperations.h \
    $$PWD/operations/operation.h \
    $$PWD/buildtree.h \
    $$PWD/io/psaver.h \
    $$PWD/ppicker.h \
    $$PWD/frustrum/bodyinfo.h \
    $$PWD/demo.h \
    $$PWD/frustrum/godatacollector.h \
    $$PWD/partitionmanager.h \
    $$PWD/operations/blockoperations.h

SOURCES += \
    $$PWD/entitymanager.cpp \
    $$PWD/psession.cpp \
    $$PWD/frustrum/frustrumdelta.cpp \
    $$PWD/frustrum/frustrumgo.cpp \
    $$PWD/operations/booleanoperations.cpp \
    $$PWD/sketchs/sketcher.cpp \
    $$PWD/operations/constractiveoperations.cpp \
    $$PWD/operations/operation.cpp \
    $$PWD/buildtree.cpp \
    $$PWD/io/psaver.cpp \
    $$PWD/ppicker.cpp \
    $$PWD/frustrum/bodyinfo.cpp \
    $$PWD/demo.cpp \
    $$PWD/frustrum/frustrum.cpp \
    $$PWD/frustrum/frusrumgofunctions.cpp \
    $$PWD/frustrum/godatacollector.cpp \
    $$PWD/partitionmanager.cpp \
    $$PWD/operations/blockoperations.cpp
