#include "buildtree.h"

#include <QVector>

using namespace pk;

BuildTree::BuildTree(QObject *parent) :
    QObject(parent)
{

}

void BuildTree::addOperation(Operation operation)
{
    mutex.lock();
    m_operations.append(operation);
    mutex.unlock();
    emit operationAdded();
}

QVector<Operation> BuildTree::getOperations()
{
    return m_operations;
}

