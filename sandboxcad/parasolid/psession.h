#ifndef PSESSION_H
#define PSESSION_H

#include "parasolid_kernel.h"

#include <QMap>
#include <QString>

#include "partitionmanager.h"

namespace pk {

class PSession {
  public:
    static PSession& Instance();
    bool createSession();
    bool stopSession();

  private:
    PSession();
    static void startFrustrum(int *ifail);
    static void stopFrustrum(int *ifail);
    static void getMemory(int *nBytes, char **memory, int *ifail);
    static void returnMemory(int *nBytes, char **memory, int *ifail);
    static PK_ERROR_code_t PKerrorHandler(PK_ERROR_sf_t *error);
};
}


#endif // PSESSION_H
