#ifndef PPICKER_H
#define PPICKER_H

#include <QVector>
#include <Qt3DCore/QRay3D>

#include "partitionmanager.h"

class PPicker
{
public:
    enum type {
        EDGE, FACE, VERTEX
    };

    static QVector<int> pickElements(Qt3D::QRay3D ray, double radius = 0);
};

#endif // PPICKER_H
