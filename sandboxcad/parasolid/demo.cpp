#include "demo.h"

#include <QVector>
#include <QVariant>

#include "easylogging++.h"
#include "parasolid_kernel.h"
#include "operations/constractiveoperations.h"
#include "operations/operation.h"
#include "io/psaver.h"
#include "ppicker.h"
#include "frustrum/godatacollector.h"

using namespace pk;

void ColourFaces(PK_BODY_t body, double colour[3])
{
    /* Colours a given body using the colour specified by the input.
    Creates an attribute, locates all the faces on the body and then
    applies the attribute.*/

    PK_ATTDEF_t colour_attdef=0;
    PK_ATTRIB_t colour_attrib=0;
    PK_FACE_t *faces=NULL;
    int n_faces=0,  n_attribs=0;
    int i=0;

    PK_BODY_ask_faces(body, &n_faces, &faces); //locate the faces.
    PK_ATTDEF_find("SDL/TYSA_COLOUR", &colour_attdef);

    for(i=0;i<n_faces;i++)
    {
        //Check if any colour attribs are present.
        n_attribs=0;
        PK_ENTITY_ask_attribs(faces[i],colour_attdef,&n_attribs,NULL);
        if(n_attribs>0)
        {
            //Remove any existing colour attribs.
            PK_ENTITY_delete_attribs(faces[i],colour_attdef,&n_attribs);
        }

        //Add new colour attribute.
        PK_ATTRIB_create_empty(faces[i], colour_attdef, &colour_attrib);
        PK_ATTRIB_set_doubles(colour_attrib, 0, 3, colour);
    }

    PK_MEMORY_free(faces);
}
void ColourEdges(PK_BODY_t body, double colour[3])
{
    /* Colour a given body using the colour specified by the input.
    Creates an attribute, locates all the faces on the body and then
    applies the attribute.*/

    PK_ATTDEF_t colour_attdef=0;
    PK_ATTRIB_t colour_attrib=0;
    PK_EDGE_t *edges=NULL;
    int n_edges=0;
    int i=0, n_attribs=0;

    PK_BODY_ask_edges(body, &n_edges, &edges); //locate the faces.
    PK_ATTDEF_find("SDL/TYSA_COLOUR", &colour_attdef);
    //Colour all of the faces on the cube.
    for(i=0;i<n_edges;i++)
    {
        n_attribs=0;
        PK_ENTITY_ask_attribs(edges[i],colour_attdef,&n_attribs,NULL);
        if(n_attribs>0)
        {
            PK_ENTITY_delete_attribs(edges[i],colour_attdef,&n_attribs);
        }
        PK_ATTRIB_create_empty(edges[i], colour_attdef, &colour_attrib);
        PK_ATTRIB_set_doubles(colour_attrib, 0, 3, colour);
    }

    //Free memory used for edges.
    PK_MEMORY_free(edges);
}
void ColourBody(PK_BODY_t body, double colour[3])
{
    //Colours the faces and edges of a body.
    ColourFaces(body,colour);
    ColourEdges(body,colour);
}




void Demo::demo1()
{
    PK_BODY_t body;
    PK_AXIS2_sf_s ax;
    ax.axis = { 1, 0, 0 };
    ax.ref_direction = { 0, 1, 0 };
    ax.location = { 0, 0, 0 };
    PK_BODY_create_solid_block(5, 5, 5, &ax, &body);
    //auto &&data = EntityManager::Instance().getBodies();
    PK_EDGE_t *edges;
    int nEdges;
    PK_BODY_ask_edges(body, &nEdges, &edges);
    ConstractiveOperations::makeChamfer({edges[1]}, 1);
    ConstractiveOperations::makeBlend({edges[6]}, 1);

    QVector<int> pickedElements = PPicker::pickElements(Qt3D::QRay3D(QVector3D(0, 0, 0)));
    LOG(TRACE) << "Picked elements: " << pickedElements;
}


void Demo::demo2()
{
    PK_BODY_t wire;
    PK_BODY_t sheet;
    PK_BODY_t sheet_rectangle[3], rectangle;
    PK_BODY_t pillar[3];
    PK_BODY_t solid;
    PK_BODY_t *united_body;
    PK_BODY_t circle;
    PK_FACE_t face;
    PK_BODY_t extruded_body;
    int n_bodies = 0;
    int finished = 0;
    PK_AXIS1_sf_t first_basis_set;
    PK_AXIS2_sf_t basis_set;
    PK_INTERVAL_t interval;
    int *edge_index = NULL;
    int        n_new_edges = 0;
    PK_EDGE_t  *new_edges = NULL;
    PK_CURVE_make_wire_body_o_t   options;
    PK_VECTOR1_t path;
    PK_BODY_extrude_o_t extrude_opts;
    PK_TOPOL_track_r_t  tracking;
    PK_TOPOL_local_r_t  results;

    PK_LINE_sf_t line_sf;
    PK_LINE_t line;

    first_basis_set.location.coord[0] = 0.;
    first_basis_set.location.coord[1] = 0.;
    first_basis_set.location.coord[2] = 0.;
    first_basis_set.axis.coord[0] = 1;
    first_basis_set.axis.coord[1] = 0;
    first_basis_set.axis.coord[2] = 0;
    line_sf.basis_set = first_basis_set;
    PK_LINE_create( &line_sf, &line );

    interval.value[0] = -5.;
    interval.value[1] = 5;

    PK_CURVE_make_wire_body_o_m( options );
    PK_CURVE_make_wire_body_2(1, &line, &interval, &options, &wire, &n_new_edges, &new_edges, &edge_index);
    if(n_new_edges)
    {
        PK_MEMORY_free(new_edges);
        PK_MEMORY_free(edge_index);
    }

    path.coord[0] = 0.;
    path.coord[1] = 0.;
    path.coord[2] = 1.;
    PK_BODY_extrude_o_m( extrude_opts );
    extrude_opts.end_bound.distance = 6.;
    PK_BODY_extrude(wire, path, &extrude_opts, &extruded_body, &tracking, &results);
    PK_TOPOL_track_r_f(&tracking);
    PK_TOPOL_local_r_f(&results);
    PK_ENTITY_delete(1, &wire);
    sheet = extruded_body;

    int i;
    PK_TOPOL_t topol; PK_SURF_t surf;PK_PLANE_sf_t plane_sf;
    path.coord[0] = 0.;
    path.coord[1] = 1.;
    path.coord[2] = 0.;
    PK_BODY_extrude_o_m( extrude_opts );
    extrude_opts.end_bound.distance = 3.;
    PK_BODY_extrude(sheet, path, &extrude_opts, &extruded_body, &tracking, &results);

    for(i=0;i<tracking.n_track_records;i++)
    {
        topol = tracking.track_records[i].product_topols[0];
        PK_CLASS_t pkclass;
        PK_ENTITY_ask_class(topol, &pkclass);
        if(pkclass==PK_CLASS_face)
        {
            PK_FACE_ask_surf(topol, &surf);
            PK_ENTITY_ask_class(surf, &pkclass);
            if(pkclass==PK_CLASS_plane)
            {
                PK_PLANE_ask(surf, &plane_sf);
                if(plane_sf.basis_set.location.coord[1] == 3  &&
                        ( plane_sf.basis_set.axis.coord[1] == 1 || plane_sf.basis_set.axis.coord[1] == -1))
                {
                    face = topol;
                    break;
                }
            }
        }
    }

    PK_TOPOL_track_r_f(&tracking);
    PK_TOPOL_local_r_f(&results);
    PK_ENTITY_delete(1, &sheet);
    solid = extruded_body;

    basis_set.location.coord[0] = -3;
    basis_set.location.coord[1] = 10;
    basis_set.location.coord[2] = 3;
    basis_set.axis.coord[0] = 0;
    basis_set.axis.coord[1] = 1;
    basis_set.axis.coord[2] = 0;
    basis_set.ref_direction.coord[0] = 1;
    basis_set.ref_direction.coord[1] = 0;
    basis_set.ref_direction.coord[2] = 0;
    PK_BODY_create_sheet_rectangle( 1.0, 1.0, &basis_set, sheet_rectangle );
    basis_set.location.coord[0] = 0;
    PK_BODY_create_sheet_rectangle( 1.0, 1.0, &basis_set, sheet_rectangle+1 );
    basis_set.location.coord[0] = 3;
    PK_BODY_create_sheet_rectangle( 1.0, 1.0, &basis_set, sheet_rectangle+2 );

    path.coord[0] = 0.;
    path.coord[1] = -1.;
    path.coord[2] = 0.;
    PK_BODY_extrude_o_m( extrude_opts );
    extrude_opts.end_bound.bound = PK_bound_face_c ;
    extrude_opts.end_bound.entity = face;

    PK_BODY_extrude(sheet_rectangle[0], path, &extrude_opts, &extruded_body, &tracking, &results);
    PK_TOPOL_track_r_f(&tracking);
    PK_TOPOL_local_r_f(&results);
    pillar[0] = extruded_body;
    PK_ENTITY_delete(1, sheet_rectangle);

    PK_BODY_extrude(sheet_rectangle[1], path, &extrude_opts, &extruded_body, &tracking, &results);
    PK_TOPOL_track_r_f(&tracking);
    PK_TOPOL_local_r_f(&results);
    pillar[1] = extruded_body;
    PK_ENTITY_delete(1, sheet_rectangle+1);

    PK_BODY_extrude(sheet_rectangle[2], path, &extrude_opts, &extruded_body, &tracking, &results);
    PK_TOPOL_track_r_f(&tracking);
    PK_TOPOL_local_r_f(&results);
    pillar[2] = extruded_body;
    PK_ENTITY_delete(1, sheet_rectangle+2);

    PK_BODY_unite_bodies(solid, 3, pillar, &n_bodies, &united_body);
    basis_set.location.coord[0] = -8;
    basis_set.location.coord[1] = 6;
    basis_set.location.coord[2] = 3;
    basis_set.axis.coord[0] = 1;
    basis_set.axis.coord[1] = 0;
    basis_set.axis.coord[2] = 0;
    basis_set.ref_direction.coord[0] = 0;
    basis_set.ref_direction.coord[1] = 1;
    basis_set.ref_direction.coord[2] = 0;

    PK_BODY_create_sheet_circle( 0.5, &basis_set, &circle);

    path.coord[0] = 1.;
    path.coord[1] = 0.;
    path.coord[2] = 0.;
    PK_BODY_extrude_o_m( extrude_opts );
    extrude_opts.end_bound.bound = PK_bound_body_c ;
    extrude_opts.end_bound.entity = united_body[0];
    extrude_opts.end_bound.nth_division = 3;

    PK_BODY_extrude(circle, path, &extrude_opts, &extruded_body, &tracking, &results);
    PK_TOPOL_track_r_f(&tracking);
    PK_TOPOL_local_r_f(&results);
    PK_ENTITY_delete(1, &circle);

    basis_set.location.coord[0] = 8;
    basis_set.location.coord[1] = 8;
    basis_set.location.coord[2] = 3;
    basis_set.axis.coord[0] = 1;
    basis_set.axis.coord[1] = 0;
    basis_set.axis.coord[2] = 0;
    basis_set.ref_direction.coord[0] = 0;
    basis_set.ref_direction.coord[1] = 1;
    basis_set.ref_direction.coord[2] = 0;

    PK_BODY_create_sheet_circle( 0.5, &basis_set, &circle);

    path.coord[0] = -1.;
    path.coord[1] = 0.;
    path.coord[2] = 0.;
    PK_BODY_extrude_o_m( extrude_opts );
    extrude_opts.start_bound.bound = PK_bound_body_c ;
    extrude_opts.start_bound.entity = united_body[0];
    extrude_opts.start_bound.nth_division = 1;
    extrude_opts.end_bound.bound = PK_bound_body_c ;
    extrude_opts.end_bound.entity = united_body[0];
    extrude_opts.end_bound.nth_division = 3;

    PK_BODY_extrude(circle, path, &extrude_opts, &extruded_body, &tracking, &results);
    PK_TOPOL_track_r_f(&tracking);
    PK_TOPOL_local_r_f(&results);
    PK_ENTITY_delete(1, &circle);

    basis_set.location.coord[0] = 5;
    basis_set.location.coord[1] = 14;
    basis_set.location.coord[2] = 3;
    basis_set.axis.coord[0] = 0.70710678118654752440084436210485;
    basis_set.axis.coord[1] = 0.70710678118654752440084436210485;
    basis_set.axis.coord[2] = 0;
    basis_set.ref_direction.coord[0] = 0;
    basis_set.ref_direction.coord[1] = 0;
    basis_set.ref_direction.coord[2] = 1;
    PK_BODY_create_sheet_rectangle( 1.0, 1.0, &basis_set, &rectangle );

    path.coord[0] = -0.70710678118654752440084436210485;
    path.coord[1] = -0.70710678118654752440084436210485;
    path.coord[2] = 0.;
    PK_BODY_extrude_o_m( extrude_opts );
    extrude_opts.end_bound.bound = PK_bound_body_c ;
    extrude_opts.end_bound.entity = united_body[0];

    PK_BODY_extrude(rectangle, path, &extrude_opts, &extruded_body, &tracking, &results);
    PK_TOPOL_track_r_f(&tracking);
    PK_TOPOL_local_r_f(&results);
    PK_ENTITY_delete(1, &rectangle);
}

void Demo::demo3()
{
    // Option structures /////////////////////////////////
    PK_CURVE_make_wire_body_o_t make_wire_body_options;
    //////////////////////////////////////////////////////

    // Partition data ////////////////////////////////////
    PK_PARTITION_t	partition;
    static PK_PMARK_t pstart, pmark1, pmark2;
    int n_new = 0, n_mod = 0, n_del = 0;
    //////////////////////////////////////////////////////

    // General Variables /////////////////////////////////
    static PK_LINE_t line, line2, line3, line1, line4, line5, line6, line7 = PK_ENTITY_null;
    static PK_CIRCLE_t circle = PK_ENTITY_null;
    PK_LINE_sf_t line_info;
    PK_CIRCLE_sf_t circle_info;
    //////////////////////////////////////////////////////

    // Vector variables //////////////////////////////////
    PK_INTERVAL_t curve_int1 = {0, 1};
    PK_INTERVAL_t curve_int2, curve_int4, curve_int5, curve_int6, curve_int7;
    PK_INTERVAL_t curve_int3 = {0, 2*M_PI};

    // Variables for the creation of primitive geometry case 1 - 4.
    PK_VECTOR1_t location1 = {0.0, 1.0, 0.0};
    PK_VECTOR1_t axis1 = {0.0, -1.0, 0.0};
    PK_VECTOR1_t location2 = {0.0, 0.0, 0.0};
    PK_VECTOR1_t axis2 = {1.0, 0.0, 0.0};
    PK_VECTOR1_t location3 = {1.0, 0.0, 0.0};
    PK_VECTOR1_t axis3 = {0.0, 1.0, 0.0};
    PK_VECTOR_t circlecentre1 = {0.5, 1.0, 0.0};
    PK_VECTOR1_t circleaxis = {0.0, 0.0, -1.0};
    PK_VECTOR1_t ref_direction = {0.0, 1.0, 0.0};

    // Variables for the creation of primitive geometry case 5 - 6.
    PK_VECTOR1_t location4 = {1.0, 1.0, 0.0};
    PK_VECTOR1_t axis4 = {-1.0, 0.0, 0.0};
    PK_VECTOR1_t circlecentre2 = {0.5, 0.5, 0.0};

    // Variables for the creation of primitive geometry case 7 - 8.
    PK_VECTOR1_t location5 = {0.5, 1.5, 0.0};
    PK_VECTOR1_t axis5 = {1.0, 0.0, 0.0};
    PK_VECTOR1_t location6 = {0.020537862668430773, 1.1418310927316131, 0.0};
    PK_VECTOR1_t axis6 = {0.0, -1.0, 0.0};
    PK_VECTOR1_t location7 = {3.0, 1.5, 0.0};
    PK_VECTOR1_t axis7 = {-0.76605274578629767, -0.64277771482315238, 0.0};

    // Positions used for parameterisation.
    PK_VECTOR_t position1 = {1.0, 1.0, 0.0};
    PK_VECTOR_t position2 = {0.0, 1.0, 0.0};
    PK_VECTOR_t position3 = {0.020537862668430773, -1.0, 0.0};
    PK_VECTOR_t position4 = {0.020537862668430773, 1.1418310927316131, 0.0};
    PK_VECTOR_t position5 = {0.5, 1.5, 0.0};
    /////////////////////////////////////////////////////

    // Initialisations for PK_EDGE_make_faces_from_wire//
    PK_EDGE_t *input_edges = PK_ENTITY_null;
    PK_LOGICAL_t *senses = PK_ENTITY_null;
    int shared_loop[2];
    PK_FACE_t face1 = PK_ENTITY_null;
    /////////////////////////////////////////////////////

    // Initialisation for PK_CURVE_make_wire_body_2//////
    static PK_CURVE_t *curves = PK_ENTITY_null;
    static PK_INTERVAL_t *curve_intervals = PK_ENTITY_null;
    static PK_BODY_t display_body;
    int n_edges = 0;
    static PK_EDGE_t *new_edges = PK_ENTITY_null;
    int * edge_index = NULL;
    /////////////////////////////////////////////////////

    // Initialisation for PK_FACE_attach_surf_fitting////
    PK_LOGICAL_t local_check;
    PK_local_check_t result_local_check;
    /////////////////////////////////////////////////////

    //Colouring//////////////////////////////////////////
    void ColourEdges(PK_BODY_t body, double colour[3]);
    void ColourBody(PK_BODY_t body, double colour[3]);
    void ColourFaces(PK_BODY_t body, double colour[3]);
    double green[3] = {0,1,0};
    double purple[3] = {0.5,0,1};
    double cyan[3] = {0,1,1};
    double blue[3] = {0,0,1};
    ////////////////////////////////////////////////////

    // Case 1 create primitive geometry and turn into closed wire bodies.

    // Create pmark for empty session.
    PK_SESSION_ask_curr_partition(&partition);
    PK_PARTITION_make_pmark(partition, &pstart);

    // Create lines and circle that will be used to make the face.
    line_info.basis_set.axis = axis1;
    line_info.basis_set.location = location1;
    PK_LINE_create(&line_info, &line1);
    line_info.basis_set.axis = axis2;
    line_info.basis_set.location = location2;
    PK_LINE_create(&line_info, &line2);
    line_info.basis_set.axis = axis3;
    line_info.basis_set.location = location3;
    PK_LINE_create(&line_info, &line3);

    // Create circle.
    circle_info.basis_set.location = circlecentre1;
    circle_info.basis_set.axis = circleaxis;
    circle_info.basis_set.ref_direction = ref_direction;
    circle_info.radius = 0.5;
    PK_CIRCLE_create(&circle_info, &circle);

    // Create pmark1.
    PK_SESSION_ask_curr_partition(&partition);
    PK_PARTITION_make_pmark(partition, &pmark1);

    // Create a temporary array of curves and fill in the array.
    curves = new PK_CURVE_t[4];
    curves[0] = line1;
    curves[1] = line2;
    curves[2] = line3;
    curves[3] = circle;

    // Find parameter values of the circle for two 3-space positions, and set these parameter values for curve_int2.
    PK_CURVE_parameterise_vector(circle, position1, &curve_int2.value[0]);
    PK_CURVE_parameterise_vector(circle, position2, &curve_int2.value[1]);

    // Create a temporary array of curve intervals.
    curve_intervals = new PK_INTERVAL_t[4];
    curve_intervals[0] = curve_int1;
    curve_intervals[1] = curve_int1;
    curve_intervals[2] = curve_int1;
    curve_intervals[3] = curve_int2;

    // Create pmark2.
    PK_SESSION_ask_curr_partition(&partition);
    PK_PARTITION_make_pmark(partition, &pmark2);

    // Set options for PK_CURVE_create_wire_body_2.
    PK_CURVE_make_wire_body_o_m (make_wire_body_options);
    make_wire_body_options.want_edges = PK_LOGICAL_true;
    PK_CURVE_make_wire_body_2(4, curves, curve_intervals, &make_wire_body_options, &display_body, &n_edges, &new_edges, &edge_index);

    // Free memory.
    PK_MEMORY_free (edge_index);

    // Create a temporary array for senses.
    senses = new PK_LOGICAL_t[1];
    senses[0] = PK_LOGICAL_true;

    // Set value for shared loops as a negative number.
    shared_loop[0] = -1;

    // Input an edge from the closed wire body and execute PK_EDGE_make_face_from_wires.
    PK_EDGE_make_faces_from_wire(1, &new_edges[0], senses, shared_loop, &face1);

    // Create a surface to fit and attach to the face.
    local_check = PK_LOGICAL_true;
    PK_FACE_attach_surf_fitting(face1, local_check, &result_local_check);

    ColourBody(display_body, green);

    // Clear memory.
    delete senses;

    // Free memory.
    PK_MEMORY_free (new_edges);


    // Roll back to pmark2.
    PK_PMARK_goto(pmark2, &n_new, NULL, &n_mod, NULL, &n_del, NULL);

    // Find parameter values of the circle for two 3-space positions.
    PK_CURVE_parameterise_vector(circle, position1, &curve_int2.value[1]);
    PK_CURVE_parameterise_vector(circle, position2, &curve_int2.value[0]);

    // Select a different interval for the circle that selects the top half. This portion of the curve
    // runs through the seam of the curve, so the upper bound will need to be modified.
    // In a real application this would be coded so that 2pi would be added
    // automatically for cases where the interval runs through the seam.
    curve_int2.value[1] = curve_int2.value[1] + 2*M_PI; //// Add 2pi to the upper bound.
    curve_intervals[3] = curve_int2;

    // Set options for PK_CURVE_create_wire_body_2.
    PK_CURVE_make_wire_body_o_m (make_wire_body_options);
    make_wire_body_options.want_edges = PK_LOGICAL_true;
    PK_CURVE_make_wire_body_2(4, curves, curve_intervals, &make_wire_body_options, &display_body, &n_edges, &new_edges, &edge_index);

    // Clear memory.
    delete curve_intervals;
    delete curves;

    // Free memory.
    PK_MEMORY_free (edge_index);


    // Create a temporary array for senses and set the sense.
    senses = new PK_LOGICAL_t[1];
    senses[0] = PK_LOGICAL_true;

    // Set value for shared loop entry as a negative number.
    shared_loop[0] = -1;

    // Input an edge from the closed wire body and execute PK_EDGE_make_face_from_wires.
    PK_EDGE_make_faces_from_wire(1, &new_edges[0], senses, shared_loop, &face1);

    // Create a surface to fit and attach to the face.
    local_check = PK_LOGICAL_true;
    PK_FACE_attach_surf_fitting(face1, local_check, &result_local_check);

    // Colour body.
    ColourBody(display_body, cyan);

    // Clear memory.
    delete senses;

    // Free memory.
    PK_MEMORY_free (new_edges);


    // Roll back to where the lines have been created.
    PK_PMARK_goto(pmark1, &n_new, NULL, &n_mod, NULL, &n_del, NULL);

    // Create a new line that will form a square with the other lines created in case 1.
    line_info.basis_set.axis = axis4;
    line_info.basis_set.location = location4;
    PK_LINE_create(&line_info, &line4);

    // Create a new circle inside of the square.
    circle_info.basis_set.location = circlecentre2;
    circle_info.basis_set.axis = circleaxis;
    circle_info.basis_set.ref_direction = ref_direction;
    circle_info.radius = 0.2;
    PK_CIRCLE_create(&circle_info, &circle);

    // Set up a temporary array for curves and fill in the array.
    curves = new PK_CURVE_t[5];
    curves[0] = line1;
    curves[1] = line2;
    curves[2] = line3;
    curves[3] = line4;
    curves[4] = circle;

    // Set up a temporary arrray for curve intervals and fill in the array.
    curve_intervals = new PK_INTERVAL_t[5];
    curve_intervals[0] = curve_int1;
    curve_intervals[1] = curve_int1;
    curve_intervals[2] = curve_int1;
    curve_intervals[3] = curve_int1;
    curve_intervals[4] = curve_int3; // curve_int3 selects the whole of the circle from 0 to 2pi.

    // Set options for PK_CURVE_create_wire_body_2.
    PK_CURVE_make_wire_body_o_m (make_wire_body_options);
    make_wire_body_options.want_edges = PK_LOGICAL_true;
    PK_CURVE_make_wire_body_2(5, curves, curve_intervals, &make_wire_body_options, &display_body, &n_edges, &new_edges, &edge_index);

    // Clear memory.
    delete curve_intervals;
    delete curves;

    // Free memory.
    PK_MEMORY_free (edge_index);

    // Create a temporary array for input edges and choose one edge from the closed wire loop to input into PK_EDGE_make_faces_from_wire.
    input_edges = new PK_EDGE_t[2];
    input_edges[0] = new_edges[0];
    input_edges[1] = new_edges[4];

    // Create temporary array for senses.
    senses = new PK_LOGICAL_t[2];
    senses[0] = PK_LOGICAL_true;
    senses[1] = PK_LOGICAL_true;

    // Set the value for the shared loops.
    shared_loop[0] = -1;
    shared_loop[1] = 0;

    // Execute PK_EDGE_make_face_from_wires.
    PK_EDGE_make_faces_from_wire(2, input_edges, senses, shared_loop, &face1);

    // Create a surface to fit and attach to the face.
    local_check = PK_LOGICAL_true;
    PK_FACE_attach_surf_fitting(face1, local_check, &result_local_check);

    // Colour body.
    ColourBody(display_body, purple);

    // Clear memory.
    delete senses;
    delete input_edges;

    // Free memory.
    PK_MEMORY_free (new_edges);

    // Case 7 Create primitive geometry and turn into closed wire bodies.

    // Roll back to pmark for the empty session.
    PK_PMARK_goto(pstart, &n_new, NULL, &n_mod, NULL, &n_del, NULL);

    // Create a new circle with a radius of 0.5.
    circle_info.basis_set.location = circlecentre1;
    circle_info.basis_set.axis = circleaxis;
    circle_info.basis_set.ref_direction = ref_direction;
    circle_info.radius = 0.5;
    PK_CIRCLE_create(&circle_info, &circle);

    // Create lines for a new body.
    line_info.basis_set.axis = axis5;
    line_info.basis_set.location = location5;
    PK_LINE_create(&line_info, &line5);
    line_info.basis_set.axis = axis6;
    line_info.basis_set.location = location6;
    PK_LINE_create(&line_info, &line6);
    line_info.basis_set.axis = axis7;
    line_info.basis_set.location = location7;
    PK_LINE_create(&line_info, &line7);

    // Parameterise intervals for the three lines and circle.
    PK_CURVE_parameterise_vector(line5, location5, &curve_int4.value[0]);
    PK_CURVE_parameterise_vector(line5, location7, &curve_int4.value[1]);
    PK_CURVE_parameterise_vector(line6, location6, &curve_int5.value[0]);
    PK_CURVE_parameterise_vector(line6, position3, &curve_int5.value[1]);
    PK_CURVE_parameterise_vector(line7, location7, &curve_int6.value[0]);
    PK_CURVE_parameterise_vector(line7, position3, &curve_int6.value[1]);
    PK_CURVE_parameterise_vector(circle, position4, &curve_int7.value[0]);
    PK_CURVE_parameterise_vector(circle, position5, &curve_int7.value[1]);

    // Add 2pi to this interval so that the correct portion of the curve can be selected.
    // In a real application this would be coded so that 2pi would be added
    // automatically for cases where the interval runs through the seam.
    curve_int7.value[1] = curve_int7.value[1] + 2*M_PI;

    // Set up a temporary array for curves and fill in the array.
    curves = new PK_CIRCLE_t[4];
    curves[0] = line5;
    curves[1] = line6;
    curves[2] = line7;
    curves[3] = circle;

    // Set up a temporary array for curve intervals and fill in the array.
    curve_intervals = new PK_INTERVAL_t[4];
    curve_intervals[0] = curve_int4;
    curve_intervals[1] = curve_int5;
    curve_intervals[2] = curve_int6;
    curve_intervals[3] = curve_int7;

    // Set options for PK_CURVE_create_wire_body_2.
    PK_CURVE_make_wire_body_o_m (make_wire_body_options);
    make_wire_body_options.want_edges = PK_LOGICAL_true;
    PK_CURVE_make_wire_body_2(4, curves, curve_intervals, &make_wire_body_options, &display_body, &n_edges, &new_edges, &edge_index);

    // Clear memory.
    delete curve_intervals;
    delete curves;

    // Free memory.
    PK_MEMORY_free (edge_index);


    // Create temporary array for senses and set the sense.
    senses = new PK_LOGICAL_t[1];
    senses[0] = PK_LOGICAL_true;

    // Set value for shared loops as a negative number.
    shared_loop[0] = -1;

    // Input an edge from the closed wire body and execute PK_EDGE_make_face_from_wires.
    PK_EDGE_make_faces_from_wire(1, &new_edges[0], senses, shared_loop, &face1);

    // Create a surface to fit and attach to the face.
    local_check = PK_LOGICAL_true;
    PK_FACE_attach_surf_fitting(face1, local_check, &result_local_check);

    // Colour body.
    ColourBody(display_body, blue);

    // Clear memory.
    delete senses;

    // Free memory.
    PK_MEMORY_free (new_edges);
}
