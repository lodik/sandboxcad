#ifndef PARTITIONMANAGER_H
#define PARTITIONMANAGER_H

#include "parasolid_kernel.h"
#include <QMap>
#include "easylogging++.h"

namespace pk {

typedef QMap<PK_PARTITION_t, QString> Partitions;
typedef struct { PK_PARTITION_t partition; QString name; } Partition;

#define partmgr pk::PartitionManager::instance()

class PartitionManager
{    
public:
    Partition createPartition(QString name);

    void setCurrentPartition(Partition partition);
    void setCurrentPartition(PK_PARTITION_t partition);
    Partition currentPartition() const;

    Partitions partitions() const;

    static PartitionManager &instance();

private:
    PartitionManager() = default;

private:
    Partitions m_partitions;
};

}   // namespace pk

#endif // PARTITIONMANAGER_H
