#include "partitionmanager.h"

using namespace pk;

Partition PartitionManager::createPartition(QString name)
{
    PK_PARTITION_t partition;
    PK_PARTITION_create_empty(&partition);

    m_partitions.insert(partition, name);

    return { partition, name };
}

void PartitionManager::setCurrentPartition(Partition partition)
{
    PK_PARTITION_set_current(partition.partition);

    LOG(DEBUG) << "Partition changed to " << partition.name;
}

void PartitionManager::setCurrentPartition(PK_PARTITION_t partition)
{
    PK_PARTITION_set_current(partition);

    LOG(DEBUG) << "Partition changed to " << m_partitions[partition];
}

Partition PartitionManager::currentPartition() const
{
    PK_PARTITION_t partition;
    PK_SESSION_ask_curr_partition(&partition);

    return { partition, m_partitions[partition] };
}

Partitions PartitionManager::partitions() const
{
    return m_partitions;
}

PartitionManager &PartitionManager::instance()
{
    static PartitionManager m_instance;
    return m_instance;
}
