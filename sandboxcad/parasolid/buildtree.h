#ifndef BUILDTREE_H
#define BUILDTREE_H

#include <QObject>
#include <QMutex>
#include <QVector>
#include "operations/operation.h"

namespace pk {

class BuildTree : public QObject
{
    Q_OBJECT
public:
    BuildTree(QObject * parent = 0);
    void addOperation(Operation operation);
    QVector<Operation> getOperations();

signals:
    void operationAdded();

private:
    QVector<Operation> m_operations;
    QMutex mutex;
};
}

#endif // BUILDTREE_H
