#ifndef SKETCHER_H
#define SKETCHER_H


class Sketcher
{
public:
    Sketcher();
    virtual void notImplemented() = 0;
};

#endif // SKETCHER_H
