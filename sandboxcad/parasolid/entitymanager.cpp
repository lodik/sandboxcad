#include "entitymanager.h"
#include "easylogging++.h"

#include "parasolid_kernel.h"
#include <QVector>

#include "psession.h"

using namespace pk;


EntityManager &EntityManager::Instance()
{
    static EntityManager m_instance;
    return m_instance;
}

void EntityManager::update()
{

}

QVector<PK_BODY_t> EntityManager::getBodies()
{
    PK_BODY_t *bodies;
    int nBodies;
    PK_PARTITION_ask_bodies(partmgr.currentPartition().partition, &nBodies, &bodies);
    QVector<PK_BODY_t> result(nBodies);
    LOG(TRACE) << "Partition \"" << partmgr.currentPartition().name << "\" contains " << nBodies << " bodies";
    for (int i = 0; i < nBodies; i++) {
        result[i] = bodies[i];
    }
    return qMove(result);
}

QVector<PK_EDGE_t> EntityManager::getEdges()
{
    auto bodies = getBodies();

    QVector<PK_EDGE_t> edges;
    PK_EDGE_t *tmpedges;
    int nEdges;

    for(auto body : bodies) {
        PK_BODY_ask_edges(body, &nEdges, &tmpedges);
        for(int i = 0; i < nEdges; i++)
            edges.append(tmpedges[i]);
    }

    return edges;
}

QVector<PK_GEOM_t> EntityManager::getGeom()
{
    PK_GEOM_t *geometry;
    int nGeometry;
    PK_PARTITION_ask_geoms(partmgr.currentPartition().partition, &nGeometry, &geometry);
    QVector<PK_GEOM_t> result(nGeometry);
    LOG(TRACE) << "Partition \"" << partmgr.currentPartition().name
                  << "\" contains " << nGeometry << " geom entitys";
    for (int i = 0; i < nGeometry; i++) {
        result[i] = geometry[i];
    }
    return qMove(result);
}

BuildTree &EntityManager::getBuildTree()
{
    return m_buildTree;
}

EntityManager::EntityManager()
{
    LOG(TRACE) << "Entity manager created";
}
