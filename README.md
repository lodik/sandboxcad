## Build instructions:
1. Install parasolid library
    + *Windows: 
        * copy all files from x64_win/base/dll (x64 bit system) or from intel_nt to vendor/parasolid/lib folder, all headers from x64_win/base to vendor/parasolid/include
        * set %PARASOLID_DIR% variable with path to /parasolid