# store stuff here
OBJECTS_DIR = $$BUILD_DIR/.obj
MOC_DIR = $$BUILD_DIR/.moc
RCC_DIR = $$BUILD_DIR/.qrc
UI_DIR = $$BUILD_DIR/.ui

# c++11
CONFIG += c++11

DEFINES += ELPP_QT_LOGGING
